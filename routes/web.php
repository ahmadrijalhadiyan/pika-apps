<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScanController;
use App\Http\Controllers\KayuController;
use App\Http\Controllers\MasterKayuController;
use App\Http\Controllers\MasterWilayahController;
use App\Http\Controllers\Configs\UserController;
use App\Http\Controllers\Configs\ModuleController;
use App\Http\Controllers\Configs\RoleController;
use App\Http\Controllers\LokasiController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('pembeli/pindai/kode', [ScanController::class, 'show']);
Route::get ('/register-pegawai', [RegisterController::class, 'index']);
Route::post ('/post-register-pegawai', [RegisterController::class, 'store']);
Route::get ('/logins', [AuthController::class, 'index']);
Route::post ('/post_login', [AuthController::class, 'postlogin']);
Route::post ('/logout-pegawai', [AuthController::class, 'logout'])->name('logout_pegawai');
Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
    Route::prefix('config')->group(function () {
        Route::get('module', [ModuleController::class, 'index']);
        Route::any('module/ajax-list', [ModuleController::class, 'ajaxList']);
        Route::get('module/create', [ModuleController::class, 'getCreate']);
        Route::post('module/create', [ModuleController::class, 'postCreate']);
        Route::get('module/show/{id}', [ModuleController::class, 'show']);
        Route::any('module/ajax-permission/{id}', [ModuleController::class, 'ajaxPermission']);
        Route::get('module/add-permission/{id}', [ModuleController::class, 'getAddPermission']);
        Route::post('module/add-permission/{id}', [ModuleController::class, 'postAddPermission']);
        Route::post('module/delete-permission/{id}/{permissionId}', [ModuleController::class, 'postDeletePermission']);
        Route::get('module/edit/{id}', [ModuleController::class, 'getUpdate']);
        Route::post('module/edit/{id}', [ModuleController::class, 'postUpdate']);
        Route::get('module/delete/{id}', [ModuleController::class, 'postDelete']);

        Route::get('role', [RoleController::class, 'index']);
        Route::any('role/ajax-list', [RoleController::class, 'ajaxList']);
        Route::get('role/create', [RoleController::class, 'create']);
        Route::post('role/create', [RoleController::class, 'store']);
        Route::get('role/show/{id}', [RoleController::class, 'show']);
        Route::any('role/ajax-user/{id}', [RoleController::class, 'ajaxUser']);
        Route::get('role/add-user/{id}', [RoleController::class, 'getAddUser']);
        Route::post('role/add-user/{id}', [RoleController::class, 'postAddUser']);
        Route::post('role/delete/{id}', [RoleController::class, 'postDeleteUser']);
        Route::get('role/edit/{id}', [RoleController::class, 'edit']);
        Route::post('role/edit/{id}', [RoleController::class, 'update']);

        Route::get('user', [UserController::class, 'index']);
        Route::any('user/ajax-list', [UserController::class, 'ajaxList']);
        Route::get('user/create', [UserController::class, 'create']);
        Route::post('user/create', [UserController::class, 'store']);
        Route::get('user/show/{id}', [UserController::class, 'show']);
        Route::any('user/ajax-role/{id}', [UserController::class, 'ajaxRole']);
        Route::post('user/delete-role/{id}/{roleId}', [UserController::class, 'postDeleteRole']);
        Route::get('user/edit/{id}', [UserController::class, 'edit']);
        Route::post('user/edit/{id}', [UserController::class, 'update']);
        Route::delete('user/delete/{id}', [UserController::class, 'destroy']);
        Route::get('user/cari', [UserController::class, 'loadDataHcms']);
        Route::post('user/import_excel', [UserController::class, 'import_excel']);
        Route::get('user/changeStatus', [UserController::class, 'changeStatus']);

        Route::get('lokasi', [LokasiController::class, 'index']);
        Route::post('/lokasi/{kodeLokasi}',[LokasiController::class, 'update']);
        Route::delete('/lokasi/hapus/{id}',[LokasiController::class, 'destroy']);

        Route::get('provinsi', [MasterWilayahController::class, 'propinsi']);
        Route::post('/provinsi/{kodeLokasi}',[MasterWilayahController::class, 'propinsi_update']);
        Route::delete('/provinsi/hapus/{id}',[MasterWilayahController::class, 'propinsi_hapus']);

        Route::get('kota', [MasterWilayahController::class, 'kota']);
        Route::post('/kota/{kodeLokasi}',[MasterWilayahController::class, 'kota_update']);
        Route::delete('/kota/hapus/{id}',[MasterWilayahController::class, 'kota_hapus']);

        Route::get('kabupaten', [MasterWilayahController::class, 'kabupaten']);
        Route::post('/kabupaten/{kodeLokasi}',[MasterWilayahController::class, 'kabupaten_update']);
        Route::delete('/kabupaten/hapus/{id}',[MasterWilayahController::class, 'kabupaten_hapus']);

        Route::any('lokasi/ajax-list', [LokasiController::class, 'ajaxList']);


        Route::get('/asal-kayu',[MasterKayuController::class, 'asal_kayu'])->name('asal-kayu');
        Route::post('/asal-kayu',[MasterKayuController::class, 'asal_kayu_update']);
        Route::delete('/asal-kayu/hapus/{id}',[MasterKayuController::class, 'asal_kayu_hapus']);

        Route::get('/cacad-kayu', [MasterKayuController::class, 'cacad_kayu'])->name('cacad-kayu');
        Route::post('/cacad-kayu',[MasterKayuController::class, 'cacad_kayu_update']);
        Route::delete('/cacad-kayu/hapus/{id}',[MasterKayuController::class, 'cacad_kayu_hapus']);

        Route::get('/jenis-kayu', [MasterKayuController::class, 'jenis_kayu'])->name('jenis-kayu');
        Route::post('/jenis-kayu',[MasterKayuController::class, 'jenis_kayu_update']);
        Route::delete('/jenis-kayu/hapus/{id}',[MasterKayuController::class, 'jenis_kayu_hapus']);

    });

    Route::get('/pindai', [ScanController::class, 'index'])->name('pindai');
    Route::get('/pindai/kode', [ScanController::class, 'show']);

    Route::prefix('data')->group(function () {
        Route::get('/kayu', [KayuController::class, 'index'])->name('kayu');
        Route::any('/kayu/ajax-list', [KayuController::class, 'ajaxList']);
        Route::get('/kayu/show/{id}',[KayuController::class, 'show']);
        Route::delete('/kayu/delete/{id}',[KayuController::class, 'destroy']);
        Route::get('/kayu/print-pdf',[KayuController::class, 'cetak_pdf']);
        Route::get('/kayu/bkph/ajax-byJenis/{jenis}',[KayuController::class, 'bkph']);
        Route::get('/kayu/tahun/ajax-byBkph/{bkph}',[KayuController::class, 'tahun']);
        Route::get('/kayu/lhp/ajax-byTahun/{jenis}/{bkph}/{tahun}',[KayuController::class, 'lhp']);
    });

    Route::get('totalpengguna',[HomeController::class, 'totalpengguna']);
    Route::get('totaljeniskayu',[HomeController::class, 'totaljeniskayu']);
    Route::get('totalasalkayu',[HomeController::class, 'totalasalkayu']);
    Route::get('totaljeniscacad',[HomeController::class, 'totaljeniscacad']);
});
