<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pika Apps - Aplikasi Pindai Kayu</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('assets/img/logo_pika.png')}}" rel="icon">
  <link href="{{asset('assets/img/logo_pika.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('assets2/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{asset('assets2/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets2/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets2/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets2/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets2/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">


  <!-- Template Main CSS File -->
  <link href="{{asset('assets2/css/style.css')}}" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo me-auto">
        <h1><a href="{{url('/')}}"><img width="50" src="{{asset('assets/img/logo_pika.png')}}">Pika Apps</a></h1>
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="{{url('/')}}">Home</a></li>
          <li><a class="nav-link scrollto" href="/login">Login</a></li>
          <li><a type="button" class="nav-link scrollto register" >Register</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="fade-up">
          <div>
            <h1>Aplikasi Pindai Kayu</h1>
            <h2>Validasi QR-Code Kayu yang Kamu Beli, Apakah Asli dari Perhutani</h2>
            <a type="button" class="btn-get-started pindai" >Pindai Kayu</a>
            <a type="button" class="btn-get-started input_data">Input QR Code Kayu</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
          <img src="{{asset('assets2/img/hero-img.png')}}" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Pika Apps</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by <a href="#">Perhutani</a>
      </div>
    </div>
  </footer><!-- End Footer -->

{{-- modal untuk menampilkan syarat register --}}
  <div class="modal fade" id="register" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Syarat dan Ketentuan Pendaftaran Akun PIKA Apps</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" onClick="window.location.reload();" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h3>Jika ingin melakukan pendaftaran akun PIKA Apps:</h3>
        <ol>
          <li>
            Pastikan kamu merupakan pegawai internal perhutani.
          </li>
          <li>
            Pendaftaran menggunakan alamat <b>NIP (Nomor Induk Pegawai)</b> yang terdaftar pada masing-masing satuan kerja.
          </li>
          <li>
            Kamu bisa mendaftarkan sebagai member PIKA Apps melaui link <a href=https://layanan.perhutani.co.id/">Layanan-TI-Perhutani</a>.
          </li>
        </ol> 
      </div>
    </div>
  </div>
</div>
{{-- modal untuk menampilkan syarat register --}}

  {{-- modal untuk menampilkan camera scan qr code --}}
  <div class="modal fade" id="pindai" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pindai QR Code Kayu Disini</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" onClick="window.location.reload();" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <main>
          @if (session('status'))
              <div class="alert alert-success" role="alert">
                  {{ session('status') }}
              </div>
          @endif
              <div id="reader"></div>
              <div id="result"></div>
        </main>
      </div>
    </div>
  </div>
</div>
{{-- modal untuk menampilkan camera scan qr code --}}

{{-- modal untuk menampilkan Input qr code --}}
  <div class="modal fade" id="inputdata" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Inputkan QR Code Kayumu Disini</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" onClick="window.location.reload();" aria-label="Close"></button>
      </div>
       <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Inputkan Kode Kayumu :</label>
            <input type="text" class="form-control" id="kode">
          </div>
        </form>
      </div>

      <div class="modal-body" style="padding-top:20px; padding-bottom:10px;">
          <div id="hasil"></div>
      </div>

      <div class="modal-footer" style="justify-content: center;">
        <button type="button" id="cek" class="btn btn-primary btn-block">Cek Data</button>
      </div>
    </div>
  </div>
</div>
{{-- modal untuk menampilkan Input scan qr code --}}

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets2/vendor/aos/aos.js')}}"></script>
  <script src="{{asset('assets2/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets2/vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{asset('assets2/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('assets2/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{asset('assets2/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets2/js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5-qrcode/2.3.4/html5-qrcode.min.js" integrity="sha512-k/KAe4Yff9EUdYI5/IAHlwUswqeipP+Cp5qnrsUjTPCgl51La2/JhyyjNciztD7mWNKLSXci48m7cctATKfLlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  var hasil ;
    $('.pindai').on('click', function(){
        //alert('test');
        $('#pindai').modal('show');
        scanner.render(success, error);
        // Starts scanner
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const scanner = new Html5QrcodeScanner('reader', {
        // Scanner will be initialized in DOM inside element with id of 'reader'
        qrbox: {
            width: 200,
            height: 200,
        },  // Sets dimensions of scanning box (set relative to reader element width)
        fps: 20, // Frames per second to attempt a scan
    });




    function success(result) {
        var kode = result;
        $.ajax({
            url:"{{url('pembeli/pindai/kode')}}",
            type: 'GET',
            data:{kode:kode},
            success:function(data){
                // alert(data.success);
                if(data.status == 'true'){
                    document.getElementById('result').innerHTML = `
                   <h4 style="text-align:center"><span class="badge text-bg-success">BARCODE DITEMUKAN !</span></h4>
                  <p>
                    No Barcode : ${data.data.barcode}<br>
                    Nomor LHP : ${data.data.no_lhp}<br>
                    Kabupaten : ${data.data.dati.ket_30}<br>
                    KPH : ${data.data.lokasi.nama_lokasi}<br>
                    TPK : ${data.data.lokasitpk.nama_lokasi}<br>
                    Asal Kayu : ${data.data.asal.ket_10}<br>
                    Jenis : ${data.data.jenis.ket_30}<br>
                    Panjang : ${data.data.panjang}<br>
                    Lebar : ${data.data.lebar}<br>
                    Volume : ${data.data.volume}<br>
                  </p>
                    `;
                }else{
                    document.getElementById('result').innerHTML = `
                    <h2>${data.message}!</h2>`;
                }
            }
        });
        scanner.clear();
        document.getElementById('reader').remove();
    }

    function error(err) {
        console.error(err);
        // Prints any errors to the console
    }
  $('.input_data').on('click', function(){
    $('#inputdata').modal('show');
  });
  $('.register').on('click', function(){
    $('#register').modal('show');
  });
  $('#cek').on('click', function(){
    var kode = $("#kode").val();
    // alert(kode);
    $.ajax({
        url:"{{url('pembeli/pindai/kode')}}",
        type: 'GET',
        data:{kode:kode},
        success:function(data){
            if(data.status == 'true'){
                document.getElementById('hasil').innerHTML = `
                <h4 style="text-align:center"><span class="badge text-bg-success">BARCODE DITEMUKAN !</span></h4>
                <p>
                  No Barcode : ${data.data.barcode}<br>
                  Nomor LHP : ${data.data.no_lhp}<br>
                  Kabupaten : ${data.data.dati.ket_30}<br>
                  KPH : ${data.data.lokasi.nama_lokasi}<br>
                  TPK : ${data.data.lokasitpk.nama_lokasi}<br>
                  Asal Kayu : ${data.data.asal.ket_10}<br>
                  Jenis : ${data.data.jenis.ket_30}<br>
                  Panjang : ${data.data.panjang}<br>
                  Lebar : ${data.data.lebar}<br>
                  Volume : ${data.data.volume}<br>
                </p>
                `;
            }else{
                    document.getElementById('hasil').innerHTML = `
                    <h3 style="text-align:center"><span class="badge text-bg-danger">${data.message}!</span></h3>`;
            }
        }
    });
  });
</script>
</body>

</html>
