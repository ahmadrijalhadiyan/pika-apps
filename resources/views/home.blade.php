@extends('layouts.master')

@section('title',config('app.name'). ' Dashboard')

@section('stylesheets')
<script src="https://www.gstatic.com/charts/loader.js"></script>
@endsection

@section('page-heading')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable({!! json_encode($pak) !!});
        var data1 = google.visualization.arrayToDataTable({!! json_encode($bkph1) !!});
        var options = {
        // title: 'Total data Barcode berdasarkan Jenis',
        is3D: true,
        'height':300,

        };
        var chart = new google.visualization.PieChart(document.getElementById('kayuchart'));
        chart.draw(data, options);
        var columnchart = new google.visualization.ColumnChart(document.getElementById('bkphchart'));
        columnchart.draw(data1, options);
    }

    $.get('totalpengguna', function(data){
        console.log(data);
        $('#total-pengguna').html(data);
    });
    $.get('totaljeniskayu', function(data){
        console.log(data);
        $('#total-jenis-kayu').html(data);
    });
    $.get('totaljeniscacad', function(data){
        console.log(data);
        $('#total-jenis-cacad').html(data);
    });
    $.get('totalasalkayu', function(data){
        console.log(data);
        $('#total-asal-kayu').html(data);
    });
</script>
@endsection

@section('content')
<div class="row">

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                           Total Jenis Kayu</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="total-jenis-kayu">0</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-tree fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                            Total Jenis Cacad</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="total-jenis-cacad">0</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-tree fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                            Total Asal Kayu</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="total-asal-kayu">0</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-tree fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                            Total Pengguna Aplikasi</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="total-pengguna">0</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-6 col-lg-6">
        <div class="card shadow mb-5">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary" >Volume Kayu ( Berdasarkan Jenis Kayu )</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div id="kayuchart" ></div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card shadow mb-5">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary" >Volume Kayu ( Berdasarkan BKPH )</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div id="bkphchart" ></div>
            </div>
        </div>
    </div>

</div>
@endsection
