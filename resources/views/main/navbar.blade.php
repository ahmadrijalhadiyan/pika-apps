 <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home">
                <div class="sidebar-brand-icon">
                    <img width="65" height="65" src="{{asset('assets/img/logo_pika.png')}}">
                </div>
                <div class="sidebar-brand-text mx-3">PIKA Apps</div>
            </a>
            @if(auth()->user()->is_active == '1')
            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item @if(Request::segment(1) == 'home') active @endif">
                <a class="nav-link @if(Request::segment(1) == 'home') active @endif" href="{{url('home')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Informasi
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item @if(Request::segment(1) == 'pindai') active @endif"">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-qrcode"></i>
                    <span>Pindai Kayu</span>
                </a>
                <div id="collapseTwo" class="collapse @if(Request::segment(1) == 'pindai') show @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Pindai Kayu</h6>
                        <a class="collapse-item @if(Request::segment(1) == 'pindai') active @endif" href="{{url('pindai')}}">Pindai No Kayu</a>
                    </div>
                </div>
            </li>
            @can('datakayu-list')
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item @if(Request::segment(1) == 'data') active @endif">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fa fa-tree"></i>
                    <span>Data Kayu</span>
                </a>
                <div id="collapseUtilities" class="collapse @if(Request::segment(1) == 'data') show @endif" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Data Kayu</h6>
                        <a class="collapse-item @if(Request::segment(2) == 'kayu') active @endif" href="{{url('data/kayu')}}"">Kayu</a>
                    </div>
                </div>
            </li>
            @endcan
            
            <!-- Divider -->
            @can('masterkayu-list')
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Pengaturan
            </div>
            @endcan
            <!-- Nav Item - Pages Collapse Menu -->
            @can('masterkayu-list')
            <li class="nav-item @if(Request::segment(1) == 'config') active @endif">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Master Data</span>
                </a>
            @endcan
                <div id="collapsePages" class="collapse @if(Request::segment(1) == 'config') show @endif" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        @can('masterkayu-list')
                        <h6 class="collapse-header">Master Kayu</h6>
                        <a class="collapse-item @if(Request::segment(2) == 'asal-kayu') active @endif" href="{{url('config/asal-kayu')}}">Asal Kayu</a>
                        <a class="collapse-item @if(Request::segment(2) == 'cacad-kayu') active @endif" href="{{url('config/cacad-kayu')}}">Cacad Kayu</a>
                        <a class="collapse-item @if(Request::segment(2) == 'jenis-kayu') active @endif" href="{{url('config/jenis-kayu')}}">Jenis Kayu</a>
                        @endcan
                        @can('masterwilayah-list')
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Master Wilayah</h6>
                        <a class="collapse-item @if(Request::segment(2) == 'lokasi') active @endif" href="{{url('config/lokasi')}}">Lokasi</a>
                        <a class="collapse-item @if(Request::segment(2) == 'provinsi') active @endif" href="{{url('config/provinsi')}}">Propinsi</a>
                        <a class="collapse-item @if(Request::segment(2) == 'kota') active @endif" href="{{url('config/kota')}}">Kota</a>
                        <a class="collapse-item @if(Request::segment(2) == 'kabupaten') active @endif" href="{{url('config/kabupaten')}}">Kabupaten</a>
                        @endcan
                        
                        @can('role-list')
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Master Pengguna</h6>
                        <a class="collapse-item @if(Request::segment(2) == 'user') active @endif" href="{{url('config/user')}}">Pengguna</a>
                        <a class="collapse-item @if(Request::segment(2) == 'module') active @endif" href="{{url('config/module')}}">Permission</a>
                        <a class="collapse-item @if(Request::segment(2) == 'role') active @endif" href="{{url('config/role')}}">Role</a>
                        @endcan
                    </div>
                </div>
            </li>
            @endif
        </ul>
        <!-- End of Sidebar -->
    <!-- Content Wrapper -->
