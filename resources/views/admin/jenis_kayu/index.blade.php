@extends('layouts.master')

@section('title',config('app.name').' | Jenis Kayu')

@section('content')
<div id="content">

       <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h2>Jenis Kayu</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Master Data</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Master Kayu</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Jenis Kayu</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>List</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-12 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Jenis Kayu</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-500">
                                        Rekap Data Jenis Kayu
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-tree fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-6 mb-4">
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Rekap Data Jenis Kayu</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Kode Jenis Kayu</th>
                                            <th>Nama Kayu</th>
                                            <th>Nama Latin</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($jns_kayu as $data)
                                        <tr>
                                            <td>{{$data->kd_jns_ky}}</td>
                                            <td>{{$data->ket_30}}</td>
                                            <td>{{$data->nama_latin}}</td>
                                            <td><button class="btn btn-info btn-sm" data-toggle="modal" data-target="#EditModal{{$data->kd_jns_ky}}">Edit</button>
                                                <!-- Modal -->
                                                <div class="modal fade" id="EditModal{{$data->kd_jns_ky}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Jenis Kayu</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <form method="post" action="{{url()->current()}}">
                                                            @csrf
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label>Kode Jenis Kayu</label>
                                                                <input type="text" class="form-control" name="kd_jns_ky" value="{{$data->kd_jns_ky}}" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Jenis Kayu</label>
                                                                <input type="text" class="form-control" name="ket_30" value="{{$data->ket_30}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Latin Jenis Kayu</label>
                                                                <input type="text" class="form-control" name="nama_latin" value="{{$data->nama_latin}}">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div> |
                                                <a href="{{ url('config/jenis-kayu/hapus/'.$data->kd_jns_ky) }}" class="btn btn-danger btn-sm" data-confirm-delete="true">Hapus</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    <!-- /.container-fluid -->
</div>

<!-- End of Main Content -->
@endsection
