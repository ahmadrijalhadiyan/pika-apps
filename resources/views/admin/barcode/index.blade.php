@extends('layouts.master')

@section('title',config('app.name').' | Kayu')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')

<script>
    $("#select-jenis").change(function(){
        if (($(this).val() !== null) && ($(this).val() !== "") && ($(this).val() !== undefined) && ($(this).val().length !== 0)) {
            $.ajax({
                url: "{{ url('data/kayu/bkph/ajax-byJenis') }}" + "/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $("#select-bkph").html(data.html);
                }
            }).fail(function() {

            });
        }
    });

    $("#select-bkph").change(function(){
        if (($(this).val() !== null) && ($(this).val() !== "") && ($(this).val() !== undefined) && ($(this).val().length !== 0)) {
            $.ajax({
                url: "{{ url('data/kayu/tahun/ajax-byBkph') }}" + "/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $("#select-tahun").html(data.html);
                }
            }).fail(function() {

            });
        }
    });

    $("#select-tahun").change(function(){
        if (($(this).val() !== null) && ($(this).val() !== "") && ($(this).val() !== undefined) && ($(this).val().length !== 0)) {
            var jenis = $("#select-jenis").val();
            var bkph = $("#select-bkph").val();
            $.ajax({
                url: "{{ url('data/kayu/lhp/ajax-byTahun') }}" + "/" + jenis + "/" + bkph + "/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $("#select-lhp").html(data.html);
                }
            }).fail(function() {

            });
        }
    });

    $(function() {
        var $url = "{{ config('app.url') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $column = [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'no_lhp', name: 'no_lhp' },
            { data: 'tgl_lhp', name: 'tgl_lhp' },
            { data: 'lokasidkb.nama_lokasi', name: 'lokasidkb.nama_lokasi' },
            { data: 'lokasitpk.nama_lokasi', name: 'lokasitpk.nama_lokasi' },
            { data: 'jenis.ket_30', name: 'jenis.ket_30' },
            { data: 'asal.ket_10', name: 'asal.ket_10' },
            { data: 'volume_lhp', name: 'volume_lhp', render: $.fn.dataTable.render.number(',', '.', 3, '') },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ];

        $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! url('data/kayu/ajax-list') !!}',
                method: 'POST'
            },
            columns: $column,
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "2%"
                },
                {
                    "targets": 2,
                    "width": "100px"
                },
                {
                    "targets": 3,
                    "width": "150px"
                },
                {
                    "targets": 4,
                    "width": "150px"
                },
                {
                    "targets": 5,
                    "width": "100px"
                },
                {
                    "targets": 6,
                    "width": "100px"
                },
                {
                    "targets": 8,
                    "width": "150px"
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });
    });
</script>
@endsection

@section('content')
<div id="content">

       <!-- Begin Page Content -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h2>Barcode Kayu</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Kayu</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>List</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-12 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col-sm-4">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Rekap Data-Data Kayu</div>
                                    {{-- <div class="h5 mb-0 font-weight-bold text-gray-500">
                                        Rekap Data-Data Kayu
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col-sm-12">
                                    <form method="GET" action="{{url('data/kayu/print-pdf')}}" target="_blank" role="form">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm" name="kd_jns_ky" id="select-jenis" required>
                                                            <option value="">Pilih Jenis Kayu</option>
                                                            @foreach($jenis as $row)
                                                            <option value="{{$row->kd_jns_ky}}">{{$row->jenis->ket_30}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm" name="kode_lokasi_dkb" id="select-bkph" required>
                                                            <option value="">Pilih BKPH</option>
                                                            {{-- @foreach($bkph as $row)
                                                            <option value="{{$row->kode_lokasi_dkb}}">{{$row->lokasidkb->nama_lokasi}}</option>
                                                            @endforeach --}}
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control form-control-sm" name="kode_thn" id="select-tahun" required>
                                                            <option value="">Pilih Tahun</option>
                                                            {{-- @foreach($tahun as $row)
                                                            <option value="{{$row->tahun}}">{{$row->tahun}}</option>
                                                            @endforeach --}}
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control form-control-sm" name="no_lhp" id="select-lhp" required>
                                                            <option value="">Pilih Nomor LHP</option>
                                                            {{-- @foreach($lhp as $row)
                                                            <option value="{{$row->no_lhp}}">{{$row->no_lhp}}</option>
                                                            @endforeach --}}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" style="text-align: right">
                                                <button type="submit" class="btn btn-sm btn-primary">
                                                    <i class="fas fa-file-pdf"></i> Download PDF
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-6 mb-4">
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Rekap Data Kayu</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="table-list" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nomor LHP</th>
                                            <th>Tanggal LHP</th>
                                            <th>Nama BKPH</th>
                                            <th>Nama TPK</th>
                                            <th>Jenis Kayu</th>
                                            <th>Asal Kayu</th>
                                            <th>Volume</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@endsection

