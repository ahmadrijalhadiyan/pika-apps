@extends('layouts.master')


@section('content')
    
<div id="content">


       <!-- Begin Page Content -->
        <div class="container-fluid">
            
            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800">No LHP Kayu <label class="badge badge-info">{{$title}}</label></h1>

            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-12 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        LHP Kayu</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-500">
                                        Rekap Data LHP Kayu
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-tree fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-6 mb-4">
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data LHP Kayu <label class="badge badge-info">{{$title}}</label> </h6>
                        </div>
                
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0" searching:false>
                                    <thead>
                                        <tr>
                                             <th>No</th>
                                            <th>Nomor LHP</th>
                                            <th>Tanggal LHP</th>
                                            <th>Nama BKPH</th>
                                            <th>Nama TPK</th>
                                            <th>Jenis Kayu</th>
                                            <th>Asal Kayu</th>
                                            <th>Panjang</th>
                                            <th>Tebal</th>
                                            <th>Volume</th>
                                            <th>No Kayu Sar</th>
                                            <th>Barcode</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
                                        $i=1;
                                        ?>
                                        @foreach($barcode as $data)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$data->no_lhp}}</td>
                                            <td>{{$data->tgl_lhp}}</td>
                                            <td>{{$data->lokasidkb->nama_lokasi}}</td>
                                            <td>{{$data->lokasitpk->nama_lokasi}}</td>
                                            <td>{{$data->jenis->ket_30}}</td>
                                            <td>{{$data->asal->ket_10}}</td>
                                            <td>{{$data->panjang}}</td>
                                            <td>{{$data->lebar}}</td>
                                            <td>{{$data->volume}}</td>
                                            <td>{{$data->no_kayu_sar}}</td>
                                            <td>{{$data->barcode}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    <!-- /.container-fluid -->
</div>
    
<!-- End of Main Content -->
@endsection


