<html>
<head>
    <title>Print Laporan</title>
    <style type="text/css">
        @font-face {
            font-family: 'OpenSans'; //you can set your custom name also here..
            src: url({{ storage_path('OpenSans-Regular.ttf') }}) format('truetype');
            font-weight: 100; // use the matching font-weight here ( 100, 200, 300, 400, etc).
            font-style: normal; // use the matching font-style here
        }
        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            color: rgb(0, 0, 0);
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            color: rgb(0, 0, 0);
            text-align: center;
            line-height: 35px;
        }
        body{
            font-family:'Arial', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:10px;
            margin:0;
        }
        .container{
            margin:0 auto;
            margin-top:0px;
            padding:1mm;
            height:auto;
            width:297mm
            background-color:#fff;
        }
        tr.border_bottom td {
            border-bottom: 1px solid black;
        }
        caption{
            font-size:13pt;
            margin-bottom:10px;
        }
        table{
            page-break-inside: auto;
            margin:0 auto;
            width:auto;
            font-size:10pt;
        }
        tr {
            page-break-inside: avoid;
            page-break-after: auto;
            }
        .data{
            border:1px solid #333;

        }
        th{
            background-color: #f0f0f0;
            font-size: 10pt;
        }
        td{
            font-size: 8pt;
            padding: 5px 5px;
        }
        h4, p{
            margin:0px;
        }
    </style>
</head>
<body>
    <header>
        Our Code World
    </header>

    <footer>
        Tanggal Cetak <?php echo date("d-m-Y");?> | Dicetak Oleh : {{auth()->user()->name}}
    </footer>

    <div class="container">
        <center>
            <h3>Nomor LHP : {{$judul->no_lhp}}<br>
            Tanggal : {{date('d-m-Y',strtotime($judul->tgl_lhp))}}</h3>
        </center>
        <h3>BKPH : {{$judul->lokasidkb->nama_lokasi}}</h3>
    <hr>
    <table class="data" rules="all" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Nomor LHP</th>
                <th>Tanggal LHP</th>
                <th>Kode Lokasi</th>
                <th>Kode Lokasi DKB</th>
                <th>No Bukti DKB</th>
                <th>Barcode</th>
            </tr>
        </thead>
        <tbody>
            @php $no =0; @endphp
            @foreach ($data as $row)
            @php $no++; @endphp
            <tr>
                <td>{{$no}}</td>
                <td>{{$row->no_lhp}}</td>
                <td>{{$row->tgl_lhp}}</td>
                <td>{{$row->lokasi->nama_lokasi}}</td>
                <td>{{$row->lokasidkb->nama_lokasi}}</td>
                <td>{{$row->no_bukti_dkb}}</td>
                <td>{{$row->barcode}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <table style="width: 100%">
        <tr>
            <td style="width: 75%"></td>
            <td>Tanggal</td>
            <td>:</td>
            <td>{{date("d-m-Y")}}</td>
        </tr>
        <tr>
            <td style="width: 75%"></td>
            <td>Nama User</td>
            <td>:</td>
            <td>{{auth()->user()->name}}</td>
        </tr>
    </table>
    </div>
</body>
</html>
