@extends('layouts.master')

@section('title',config('app.name').' | Pindai Kayu')

@section('content')
<div id="content">

       <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800">Pindai Kayu</h1>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <button type="button" class="btn btn-primary pindai">
                        <i class="fa fa-qrcode"></i> Mulai Pindai Kayu
                        </button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="pindai" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pindai Barcode Kayu</h5>
                            <button type="button" class="close" onClick="window.location.reload();" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-body">
                                <main>
                                    <div id="reader"></div>
                                    <div id="result"></div>
                                </main>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5-qrcode/2.3.4/html5-qrcode.min.js" integrity="sha512-k/KAe4Yff9EUdYI5/IAHlwUswqeipP+Cp5qnrsUjTPCgl51La2/JhyyjNciztD7mWNKLSXci48m7cctATKfLlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $('.pindai').on('click', function(){
        //alert('test');
        $('#pindai').modal('show');
        scanner.render(success, error);
        // Starts scanner
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const scanner = new Html5QrcodeScanner('reader', {
        // Scanner will be initialized in DOM inside element with id of 'reader'
        qrbox: {
            width: 200,
            height: 200,
        },  // Sets dimensions of scanning box (set relative to reader element width)
        fps: 20, // Frames per second to attempt a scan
    });




    function success(result) {
        var kode = result;
        $.ajax({
            url:"{{url('/pindai/kode')}}",
            type: 'GET',
            data:{kode:kode},
            success:function(data){
                // alert(data.success);
                if(data.status == 'true'){
                    document.getElementById('result').innerHTML = `
                    <h4 style="text-align:center"><span class="badge text-bg-success">BARCODE DITEMUKAN !</span></h4>
                    <p>
                        No Barcode : ${data.data.barcode}<br>
                        Nomor LHP : ${data.data.no_lhp}<br>
                        Kabupaten : ${data.data.dati.ket_30}<br>
                        KPH : ${data.data.lokasi.nama_lokasi}<br>
                        TPK : ${data.data.lokasitpk.nama_lokasi}<br>
                        Asal Kayu : ${data.data.asal.ket_10}<br>
                        Jenis : ${data.data.jenis.ket_30}<br>
                        Panjang : ${data.data.panjang}<br>
                        Lebar : ${data.data.lebar}<br>
                        Volume : ${data.data.volume}<br>
                    </p>
                    `;
                }else{
                    document.getElementById('result').innerHTML = `
                    <h2>${data.message}!</h2>`;
                }
            }
        });
        scanner.clear();
        document.getElementById('reader').remove();
    }

    function error(err) {
        console.error(err);
        // Prints any errors to the console
    }

</script>
@endsection
