@extends('layouts.master')

@section('title', __('Forbidden'))
@section('content')
<div class="container-fluid">

    <!-- 404 Error Text -->
    <div class="text-center">
        <div class="error mx-auto" data-text="403">403</div>
        <p class="lead text-gray-800 mb-5">Forbiden</p>
        <p class="text-gray-500 mb-0">Anda Tidak Mempunyak Hak Akses</p>
        <a href="{{url('home')}}">← Back to Dashboard</a>
    </div>

</div>
@endsection
