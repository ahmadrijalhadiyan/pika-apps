@extends('layouts.master')

@section('title', config('app.name').' | Detail Module')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('scripts')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script>
    $(function() {
        var $url = "{{ config('app.url') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $column = [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ];

        $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! url('config/role/ajax-user/'.$hashed_id.'') !!}',
                method: 'POST'
            },
            columns: $column,
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "4%"
                },
                {
                    "targets": 3,
                    "width": "200px"
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });

        $(document).on('click', '.delete-btn', function() {
            var dataId = $(this).data('id');
            var dataName = $(this).data('nama');
            var deleteUrl = "{{ url('config/role/delete-user') }}" + "/" + "{{ $hashed_id }}" + "/" + dataId ;
            var csrf = "{{ csrf_token() }}";

            swal({
                text: "Hapus Data User "+ dataName +" untuk tipe user ini ?" ,
                icon: "warning",
                dangerMode: true,
                buttons: {
                    cancel: {
                        text: "Batal",
                        value: false,
                        visible: true,
                        className: "btn btn-sm btn-white"
                    },
                    confirm: {
                        text: "Hapus",
                        value: true,
                        visible: true,
                        className: "btn btn-sm btn-danger",
                        closeModal: true
                    }
                }
            }).then((value) => {
                if (value === true) {
                    $.redirect(deleteUrl, {"_token": csrf});
                }
                swal.close();
            });;
        });
    });
</script>
@endsection

@section('content')
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>Detail Role</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Role</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Detail</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h5 class="m-0 font-weight-bold text-primary">Detail Role</h5>
                        <div>
                            <a href="{{ url('config/role') }}" class="btn btn-primary btn-sm modal-form">
                                <i class="fa fa-arrow-circle-left"></i>
                                Kembali
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <dl class="row mb-0">
                                    <div class="col-sm-4 text-sm-right"><dt>Nama Role:</dt> </div>
                                    <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $role->name }}</dd></div>
                                </dl>
                                <dl class="row mb-0">
                                    <div class="col-sm-4 text-sm-right"><dt>Nama Guard:</dt> </div>
                                    <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $role->guard_name }}</dd> </div>
                                </dl>
                            </div>
                            <div class="col-lg-8" id="cluster_info">
                                <dl class="row mb-0">
                                    <div class="col-sm-4 text-sm-right"><dt>Permission:</dt> </div>
                                    <div class="col-sm-8 text-sm-left">
                                        <dd class="mb-1">
                                            @if(!empty($rolePermissions))
                                            @foreach($rolePermissions as $v)
                                                <label class="label label-success">{{ $v->name }},</label>
                                            @endforeach
                                            @endif
                                        </dd>
                                    </div>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding: 1%"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h5 class="m-0 font-weight-bold text-primary">Daftar User</h5>
                        <div>
                            <a href="{{ url('config/role/add-user/'.$hashed_id.'') }}" class="btn btn-primary btn-sm modal-form">
                                <i class="fa fa-plus"></i>
                                Tambah data user
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('layouts.flashMessage')
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-list">
                                <thead>
                                <tr>
                                    <th>#No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Email</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="padding: 1%"></div>
@endsection
