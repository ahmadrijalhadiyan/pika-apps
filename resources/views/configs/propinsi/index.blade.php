@extends('layouts.master')

@section('title',config('app.name').' | Provinsi')

@section('content')
<div id="content">

       <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h2>Jenis Propinsi</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Master Data</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Master Wilayah</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Propinsi</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>List</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-12 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Jenis Propinsi</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-500">
                                        Rekap Data Propinsi
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-tree fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-6 mb-4">
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Rekap Data Propinsi</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Kode Kabupaten</th>
                                            <th>Jenis Kabupaten</th>
                                            <th>Jenis Propinsi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($propinsi as $data)
                                        <tr>
                                            <td>{{$data->kode_dt2}}</td>
                                            <td>{{$data->ket_30}}</td>
                                            <td>{{$data->nama_propinsi}}</td>
                                            <td>
                                                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#EditModal{{$data->kode_dt2}}">Edit</button>
                                                                                                <!-- Modal -->
                                                <div class="modal fade" id="EditModal{{$data->kode_dt2}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Provinsi</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <form method="post" action="{{url('config/provinsi/'.$data->kode_dt2) }}">
                                                            @csrf
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label>Kode Dati 2</label>
                                                                <input type="text" class="form-control" name="kode_dt2" value="{{$data->kode_dt2}}" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Provinsi</label>
                                                                <input type="text" class="form-control" name="nama_propinsi" value="{{$data->nama_propinsi}}">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div> |
                                                <a href="{{ url('config/provinsi/hapus/'.$data->kode_dt2) }}" class="btn btn-danger btn-sm" data-confirm-delete="true">Hapus</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    <!-- /.container-fluid -->
</div>

<!-- End of Main Content -->
@endsection
