@extends('layouts.master')

@section('title', config('app.name').' | Config User')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')
<script>
    $(function() {
        var $url = "{{ config('app.url') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $column = [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'roles', name: 'roles' },
            { data: 'status', name: 'status' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ];

        $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! url('config/user/ajax-list') !!}',
                method: 'POST'
            },
            columns: $column,
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "2%"
                },
                {
                    "targets": 5,
                    "width": "250px"
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });
    });
</script>
<script>
    $(document).on('change', '.status', function() {
        var status = $(this).prop('checked') == true ? 1 : 0;
        var user_id = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{url('config/user/changeStatus')}}',
            data: {'status': status, 'user_id': user_id},
            success: function(data){
            console.log(data.success)
            }
        });
    });
  </script>
@endsection

@section('content')
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>Daftar Pengguna</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Pengguna</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>List</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h5 class="m-0 font-weight-bold text-primary">Daftar Pengguna</h5>
                        <div class="ibox-tools">
                            <a href="{{ url('config/user/create') }}" class="btn btn-primary btn-sm modal-form">
                                <i class="fa fa-plus"></i>
                                Tambah data
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('layouts.flashMessage')

                        <div class="table-responsive">
                            <table class="table table-striped" id="table-list">
                                <thead>
                                    <tr>
                                        <th>#No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
