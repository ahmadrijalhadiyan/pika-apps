@extends('layouts.master')

@section('title', config('app.name').' | Detail User')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('scripts')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script>
    $(function() {
        var $url = "{{ config('app.url') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $column = [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'name', name: 'name' },
            { data: 'guard_name', name: 'guard_name' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ];

        $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! url('config/user/ajax-role/'.$hashed_id.'') !!}',
                method: 'POST'
            },
            columns: $column,
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "4%"
                },
                {
                    "targets": 3,
                    "width": "170px"
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });

        $(document).on('click', '.delete-btn', function() {
            var dataId = $(this).data('id');
            var dataName = $(this).data('nama');
            var deleteUrl = "{{ url('config/user/delete-role') }}" + "/" + "{{ $hashed_id }}" + "/" + dataId ;
            var csrf = "{{ csrf_token() }}";

            new swal({
                text: "Hapus Data tipe user "+ dataName +" untuk user ini ?" ,
                icon: "warning",
                dangerMode: true,
                buttons: {
                    cancel: {
                        text: "Batal",
                        value: false,
                        visible: true,
                        className: "btn btn-sm btn-white"
                    },
                    confirm: {
                        text: "Hapus",
                        value: true,
                        visible: true,
                        className: "btn btn-sm btn-danger",
                        closeModal: true
                    }
                }
            }).then((value) => {
                if (value === true) {
                    $.redirect(deleteUrl, {"_token": csrf});
                }
                swal.close();
            });;
        });
    });
</script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Detail User</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="/config/user">Pengguna</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Detail</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
                <div class="col-md-4">
                    <div class="profile-image">
                        <img src="{{asset('img/avatar.png') }}" width="50" class="rounded-circle circle-border m-b-md" alt="profile">
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">
                                    {{ $user->name }}
                                </h2>
                                <h4>{{ $user->employee->npk ?? ''}}</h4>
                                <h5>{{ $user->employee->position ?? ''}}</h5>
                                <small>
                                    <a href="{{ url('config/user/') }}" class="btn btn-primary btn-sm modal-form"><i class="fa fa-arrow-circle-left"></i>Kembali </a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row small m-b-xs">
                        <div class="col-lg-6">
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Email:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->email }}</dd> </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Nomor Telepon:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->telp }}</dd> </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Department:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->employee->department ?? '' }}</dd> </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Divisi:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->employee->division ?? '' }}</dd> </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Site:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->employee->site ?? '' }}</dd> </div>
                            </dl>
                        </div>
                        <div class="col-lg-6">
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Status:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">
                                    @if($user->is_active == 1)
                                    <label class="label label-success">Aktif</label>
                                    @else
                                    <label class="label label-warning">Non Aktif,</label>
                                    @endif
                                </dd></div>
                            </dl>
                        </div>
                    </div>
                </div>
    </div>

    <div class="row">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Daftar Tipe Pengguna</h5>
                    <div>
                        <a href="{{ url('config/role/add-user/'.$user->id) }}" class="btn btn-primary btn-sm modal-form">
                            <i class="fa fa-plus"></i>
                            Tambah data tipe pengguna
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @include('layouts.flashMessage')

                    <div class="table-responsive">
                        <table class="table table-striped" id="table-list">
                            <thead>
                            <tr>
                                <th>#No</th>
                                <th>Nama</th>
                                <th>Platform</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Aktifitas</h5>
                </div>

                <div class="ibox-content sk-loading">
                    <div class="activity-stream">

                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
@endsection
