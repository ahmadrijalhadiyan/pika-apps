@extends('layouts.master')

@section('title', config('app.name').' | User Profile')

@section('scripts')
<script type="text/javascript">
    $(document).ready(function (e) {
       $('#image').change(function(){
        let reader = new FileReader();
        reader.onload = (e) => {
          $('#preview-image-before-upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
       });
    });
    </script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>User</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Profile</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    @include('layouts.flashMessage')
    <div class="row animated fadeInRight">
        <div class="col-md-4">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Profile</h5>
                </div>
                <div>
                    <div class="ibox-content no-padding border-left-center">
                        @php $path = storage_path('app/public/img/'.$user->avatar);
                            $isExists = File::exists($path);
                        @endphp
                        @if($isExists > 0)
                            <center><img alt="image" data-toggle="modal" data-target="#uploadimage" width="200px" class="img-fluid" src="{{asset('storage/public/img/'.$user->avatar)}}"></center>
                        @else
                            <center><img alt="image" data-toggle="modal" data-target="#uploadimage" width="200px" class="img-fluid" src="{{asset('img/avatar.png')}}"></center>
                        @endif
                    </div>
                    <div class="ibox-content profile-content">
                        <h4><strong>{{$user->employee->nama ?? ''}}</strong></h4>
                        <p>{{$user->employee->npk ?? ''}}</p>
                        <h5>
                            About me
                        </h5>
                        <p>
                            {{$user->employee->position ?? ''}}<br>
                            {{$user->employee->section ?? ''}}<br>
                            {{$user->employee->department ?? ''}}<br>
                            {{$user->employee->division ?? ''}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Detail</h5>
                    <div class="ibox-tools">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ubahpassword"><i class="fa fa-key"></i> Edit Profil</button>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <dl class="row mb-0">
                                <div class="col-sm-3 text-sm-left"><dt>Nama</dt> </div>
                                <div class="col-sm-1 text-sm-left"><dt>:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->employee->nama ?? '' }}</dd></div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-3 text-sm-left"><dt>NPK</dt> </div>
                                <div class="col-sm-1 text-sm-left"><dt>:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->employee->npk ?? '' }}</dd></div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-3 text-sm-left"><dt>Alamat Email</dt> </div>
                                <div class="col-sm-1 text-sm-left"><dt>:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $user->email }}</dd></div>
                            </dl>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="uploadimage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body">
        <form action="{{url('user/profile/simpangambar')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
            <input class="form-control" type="file" name="image" id="image">
            </div>
            <div class="col-md-12 mb-2">
                @if(Storage::disk('image')->exists(Auth::user()->avatar))
                <img id="preview-image-before-upload" src="{{asset('storage/img/'.Auth::user()->avatar)}}"
                    alt="preview image" style="max-height: 250px;">
                @else
                <img id="preview-image-before-upload" src="{{asset('img/avatar.png')}}"
                    alt="preview image" style="max-height: 250px;">
                @endif
            </div>
            <input type="submit" class="btn btn-primary"value="Upload">
        </form>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="ubahpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post" action="{{ url('user/profile/ubahpassword') }}">
        @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
            <label class="col-sm-4">Email</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="email" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4">Nomor Telp</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="telp" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4">Password Baru</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="password" required>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection
