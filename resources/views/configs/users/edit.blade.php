@extends('layouts.master')

@section('title', config('app.name').' | Edit User')

@section('stylesheets')
<link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/textSpinners/spinners.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

<script>
$('.chosen-select').chosen({width: "100%"});

$('.custom-file-input').on('change', function() {
   let fileName = $(this).val().split('\\').pop();
   $(this).next('.custom-file-label').addClass("selected").html(fileName);
});

</script>
@endsection

@section('content')
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>Ubah Data Pengguna</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Pengguna</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Ubah Data</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h5 class="m-0 font-weight-bold text-primary">Ubah Data Pengguna</h5>
                        <div>
                            <a href="{{ url()->previous() }}" class="btn btn-primary btn-xs modal-form">
                                <i class="fa fa-arrow-circle-left"></i>
                                Kembali
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('layouts.flashMessage')

                        <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data" id="form-user">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row has-warning">
                                        <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$user->name}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group row has-success">
                                        <label class="col-sm-4 col-form-label">NIP (Nomor Induk Pegawai)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm" id="nik" name="nik" value="{{ $user->nik }}">
                                        </div>
                                    </div>
                                    <div class="form-group row has-warning">
                                        <label class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control form-control-sm" id="email" name="email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group row has-warning">
                                        <label class="col-sm-4 col-form-label">Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control form-control-sm" name="password" value="perhutani">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Aktif</label>
                                        <div class="col-sm-8">
                                            <div class="switch">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" id="aktif-switch" name="aktif" value="1" @if($user->is_active == 1) checked="" @endif>
                                                    <label class="onoffswitch-label" for="aktif-switch">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tipe User</label>
                                <div class="col-sm-10">
                                    <select data-placeholder="Pilih Jenis Tipe User.." class="chosen-select" name="roles[]" multiple>
                                        <option value=""></option>
                                        @foreach ($roles as $row)
                                        <option value="{{ $row->name }}" @if(in_array($row->id, $arrayRole)) selected="" @endif>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="form-text m-b-none">Pilih jenis tipe anggota (bisa lebih dari satu).</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white btn-sm" type="reset">Cancel</button>
                                    <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
