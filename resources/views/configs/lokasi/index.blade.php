@extends('layouts.master')

@section('title',config('app.name').' | Lokasi')

@section('content')
<div id="content">

       <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h2>Jenis Lokasi</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Master Data</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Master Wilayah</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Lokasi</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>List</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-12 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Jenis Kayu</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-500">
                                        Rekap Data Jenis Kayu
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-tree fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-6 mb-4">
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Rekap Data Jenis Lokasi</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Jenis Kode Lokasi</th>
                                            <th>Kode Lokasi</th>
                                            <th>Nama Lokasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($lokasi as $data)
                                        <tr>
                                            <td>{{$data->jns_lokasi_id}}</td>
                                            <td>{{$data->kode_lokasi}}</td>
                                            <td>{{$data->nama_lokasi}}</td>
                                            <td><button class="btn btn-info btn-sm" data-toggle="modal" data-target="#EditModal{{$data->kode_lokasi}}">Edit</button>
                                                <!-- Modal -->
<div class="modal fade" id="EditModal{{$data->kode_lokasi}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Lokasi Kayu</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="post" action="{{url('config/lokasi/'.$data->kode_lokasi) }}">
            @csrf
        <div class="modal-body">
          <div class="form-group">
            <label>Kode Lokasi</label>
            <input type="text" class="form-control" name="kode_lokasi" value="{{$data->kode_lokasi}}" readonly>
          </div>
          <div class="form-group">
            <label>Nama Lokasi</label>
            <input type="text" class="form-control" name="nama_lokasi" value="{{$data->nama_lokasi}}">
          </div>
          <div class="form-group">
            <label>Jenis Lokasi ID</label>
            <input type="text" class="form-control" name="jns_lokasi_id" value="{{$data->jns_lokasi_id}}">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
      </div>
    </div>
  </div> | <a href="{{ url('config/lokasi/hapus/'.$data->kode_lokasi) }}" class="btn btn-danger btn-sm" data-confirm-delete="true">Hapus</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    <!-- /.container-fluid -->
</div>

<!-- End of Main Content -->
@endsection
