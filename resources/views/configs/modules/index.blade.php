@extends('layouts.master')

@section('title', config('app.name').' | Config Module')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('scripts')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script>
    $(function() {
        var $url = "{{ config('app.url') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $column = [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'name', name: 'name' },
            { data: 'detail', name: 'detail' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ];

        $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! url('config/module/ajax-list') !!}',
                method: 'POST'
            },
            columns: $column,
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "4%"
                },
                {
                    "targets": 3,
                    "width": "250px"
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });

        $(document).on('click', '.delete-btn', function() {
            var dataId = $(this).data('id');
            var dataName = $(this).data('nama');
            var deleteUrl = "{{ url('config/module/delete') }}" + "/" + dataId;
            var csrf = "{{ csrf_token() }}";

            Swal.fire({
                title: "Yakin hapus data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonColor: '#3085d6',
                cancelButtonText: "Batal"

            }).then(result => {
                if(result.isConfirmed){
                    window.location.href = deleteUrl
                }
            })
            return false;
        });
    });
</script>
@endsection

@section('content')
<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>Daftar Modul</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Modul</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>List</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h5 class="m-0 font-weight-bold text-primary">Daftar Modul</h5>
                        <div>
                            <div class="ibox-tools">
                                <a href="{{ url('config/module/create') }}" class="btn btn-primary btn-sm modal-form">
                                    <i class="fa fa-plus"></i>
                                    Tambah data
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('layouts.flashMessage')

                        <div class="table-responsive">
                            <table class="table table-striped" id="table-list">
                                <thead>
                                    <tr>
                                        <th>#No</th>
                                        <th>Nama</th>
                                        <th>Detail</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
