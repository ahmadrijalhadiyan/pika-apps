@extends('layouts.master')

@section('title', config('app.name').'| SiteSetting')

@section('stylesheets')
<link href="{{ asset('css/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>

<script>
$('#maintenance-mode').on('click', function(e) {
    if ($(this).is(':checked')) {
        $('#maintenance-mode-active').show();
    }else {
        $('#maintenance-mode-active').hide();
    }
});
</script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Site Settings</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Config</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Site Settings</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Site Settings</h5>
                    <div class="ibox-tools">
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-xs modal-form">
                            <i class="fa fa-arrow-circle-o-left"></i>
                            Kembali 
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    @include('layouts.flashMessage')
                            
                    <form method="post" action="{{ url()->current() }}">
                        @csrf

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Maintenance Mode</label>
                            <div class="col-sm-10">
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="maintenance-mode" name="maintenance" @if($settingDefault['maintenance_mode'] == 1) checked @endif>
                                        <label class="onoffswitch-label" for="maintenance-mode">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="maintenance-mode-active" style="display:none">
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sandi Pengaktifan</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="sandi">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Ulang Sandi Pengaktifan</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="sandi_confirmation">
                            </div>
                        </div>
                        </div>
                        
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white btn-sm" type="reset">Cancel</button>
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection