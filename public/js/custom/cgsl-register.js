// icare leader autocomplete
var leader_options = {
    url: function(phrase) {
        return leader_url;
    },
    
    getValue: function(element) {
        return element.member.nama;
    },
    
    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },
    
    preparePostData: function(data) {
        data.phrase = $("#leader_list").val();

        return data;
    },

    list: {
        maxNumberOfElements: 10,
        onSelectItemEvent: function() {
            var user_id = $("#leader_list").getSelectedItemData().id;
            
            $("#leader").val(user_id).trigger("change");
        },
        match: {
            enabled: true
        },
    },
    
    requestDelay: 200
};

$("#leader_list").easyAutocomplete(leader_options);
$(".easy-autocomplete").css('width', '');

// nama member autocomplete
var member_options = {
    url: function(phrase) {
        return member_url;
    },
    
    getValue: function(element) {
        return element.member.nama;
    },
    
    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },
    
    preparePostData: function(data) {
        data.phrase = $("#member_list").val();

        return data;
    },

    list: {
        maxNumberOfElements: 10,
        onSelectItemEvent: function() {
            var member_id = $("#member_list").getSelectedItemData().member.id;
            var email = $("#member_list").getSelectedItemData().email;
            
            $("#member").val(member_id).trigger("change");
            $("#email").val(email).trigger("change");
            $("#valid_email").val(email).trigger("change");
        },
        match: {
            enabled: true
        },
    },
    
    requestDelay: 200
};

$("#member_list").easyAutocomplete(member_options);
$(".easy-autocomplete").css('width', '');