$(function(){
    icheck();
    message_timer();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

// icare leader autocomplete
var options = {
    url: function(phrase, type) {
        return url;
    },
    
    getValue: function(element) {
        return element.member.nama;
    },
    
    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },
    
    preparePostData: function(data) {
        data.phrase = $("#leader_list").val();
        data.type = $("#topic_type").val();

        return data;
    },

    list: {
        maxNumberOfElements: 10,
        onSelectItemEvent: function() {
            var user_id = $("#leader_list").getSelectedItemData().id;
            
            $("#leader").val(user_id).trigger("change");
            $(".report-block").hide();
            $("input:checkbox").iCheck('uncheck');
        },
        match: {
            enabled: true
        },
    },
    
    requestDelay: 200
};

$("#leader_list").easyAutocomplete(options);
$(".easy-autocomplete").css('width', '');
$('.easy-autocomplete').closest('.input-group').each(function(i, inputGroup) {
    $(inputGroup).removeClass('input-group');
    $autocomplete = $(inputGroup).find('.easy-autocomplete');
    $(inputGroup).find('.input-group-addon').appendTo($autocomplete);
    $autocomplete.addClass('input-group');
});

// search student for specific leader
$("#leader-search").on('click', function() {
    var user_id = $('#leader').val();
    var topic_type_id = $('#topic_type').val();
    get_student_list(user_id, topic_type_id);
});

// menampilkan list student
function get_student_list(user_id, topic_type_id)
{
    $.post(base_url+'/cgsl/student_list', { leader_id: user_id, topic_type_id:topic_type_id }, function(data) {
        var html = '';
        var students = JSON.parse(data);
        if(data != '[]') {
            $.each(students, function(i, student) {
                html += pupulate_student_checkbox(student);
            });

            $("#student_list").html(html);
            icheck();
            $(".report-block").show();
        } else {
            html = `<div class="alert alert-danger alert-block message">
                        Data Peserta tidak ditemukan!
                    </div>`;
            $("#message").html(html);
            message_timer();
        }
    });
}

function pupulate_student_checkbox(student)
{
    html = `<div class="i-checks">
                <label>
                    <input name="member[]" type="checkbox" value="`+student.member_id+`"> <i></i> `+student.member.nama+`
                </label>
            </div>`;

    return html;
}

function icheck()
{
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
}

function message_timer()
{
    $(".message").fadeTo(2000, 500).slideUp(500, function(){
        $(".message").slideUp(500);
        $(".message").remove();
    });
}