<?php
use App\Models\Bonita;
use App\Models\PenentuanJangkaBenah;
use App\Models\NNormal;
use App\Models\HasilRisalah;
use App\Models\Anakpetak;
use App\Models\KelasUmur;
use App\Models\TabelJenis;
use App\Models\Tallysheet\Buatts;
use App\Models\Bkph;
use App\Models\Rph;
use App\Models\Kph;
use App\Models\MasterEmployee;
use App\Models\Kelasperusahaan;
use App\Models\Bagianhutan;
use App\Models\SpkTebangan;
use App\Models\RttTebangan;
use App\Models\RttPersemaian;
use App\Models\RttTanaman;
use App\Models\Rtt_Pemel1;
use App\Models\Rtt_Pemel2;
use App\Models\Rtt_Pemel4;
use App\Models\Rtt_Pemel5;
use App\Models\RttPerawatan;
use App\Models\RttSadapan;
use App\Models\Rtt\RttPersiapanTanaman;
use App\Models\BukuObor\Workorder;
use DB as DB;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Writer\ValidationException;
use App\User;

function tanggal_indonesia($tgl, $tampil_hari = false)
{
    $nama_hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
    $nama_bulan = array(
        1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
        "September", "Oktober", "November", "Desember"
    );
    if($tgl){
        $tahun = substr($tgl, 0, 4);
        $bulan = $nama_bulan[(int) substr($tgl, 5, 2)];
        $tanggal = substr($tgl, 8, 2);
        $text = "";
        if ($tampil_hari) {
            $urutan_hari = date('w', mktime(0, 0, 0, substr($tgl, 5, 2), $tanggal, $tahun));
            $hari = $nama_hari[$urutan_hari];
            $text .= $hari . ", ";
        }
        $text .= $tanggal . " " . $bulan . " " . $tahun;
        return $text;
    }
    return '................';
}

function number_indonesia($number)
{
    return number_format($number, 2, ",",".");
}

function getBonita($jenis,$umur,$peninggi)
{
    if($jenis == 30){
        return 3;
    }else{
        $bonita = Bonita::where('jenistanaman_id',$jenis)
        ->where('umur',$umur)
        ->where('peninggi',round($peninggi,1))
        ->first();
        if($bonita){
            $bonita = $bonita->bonita;
            return $bonita;
        }
        return 0;
    }
}

function getDkn($luaspu,$bonita,$jumlahpohon,$umur,$jenis,$jaraktanam = null)
{
    if($luaspu == 0.02)
    {
        $luas = 50;
    }elseif($luaspu == 0.04){
        $luas = 25;
    }elseif($luaspu == 0.10){
        $luas = 10;
    }elseif($luaspu == 0){
        $luas = 0;
    }
    if($jenis == 30){
        $pohon = NNormal::whereumur($umur)->where('jenistanaman_id',$jenis)->whereid_jaraktanam($jaraktanam)->first();
    }else{
        $pohon = TabelJenis::where('jenistanaman_id',$jenis)
        ->where('umur',$umur)
        ->where('bonita',$bonita)
        ->first();
    }
    // return $pohon->ntabel;
    // return $jumlahpohon;
    if($pohon){
        $nlap = $jumlahpohon*$luas;
        $dkn = isset($nlap) ? $nlap/$pohon->ntabel : 0;
        return number_format($dkn,2);
    }
    return 0;
}

function getKbd($luaspu,$bidangdasar,$bonita,$umur,$jenis,$jaraktanam = null)
{
    if($luaspu == 0.02)
    {
        $luas = 50;
    }elseif($luaspu == 0.04){
        $luas = 25;
    }elseif($luaspu == 0.10){
        $luas = 10;
    }elseif($luaspu == 0){
        $luas = 0;
    }
    if($jenis == 30){
        $tbljns = NNormal::whereumur($umur)->where('jenistanaman_id',$jenis)->whereid_jaraktanam($jaraktanam)->first();
    }else{
        $tbljns = TabelJenis::where('jenistanaman_id',$jenis)
        ->where('umur',$umur)
        ->where('bonita',$bonita)
        ->first();
    }
    // return $umur;
    // return $bidangdasar;
    if($tbljns){
        $luas_bds = $bidangdasar*$luas;
        if($luas_bds > 0 && $tbljns->lbds > 0){
            $kbd = isset($luas_bds) ? $luas_bds/$tbljns->lbds : 0;
            return number_format($kbd,2);
        }
        return 0;
    }
    return 0;
}

function getRomawi($nomor){
    switch ($nomor){
        case 1:
            return "I";
            break;
        case 2:
            return "II";
            break;
        case 3:
            return "III";
            break;
        case 4:
            return "IV";
            break;
        case 5:
            return "V";
            break;
        case 6:
            return "VI";
            break;
        case 7:
            return "VII";
            break;
        case 8:
            return "VIII";
            break;
        case 9:
            return "IX";
            break;
        case 10:
            return "X";
            break;
        case 11:
            return "XI";
            break;
        case 12:
            return "XII";
            break;
    }
}

function getColspan($daur,$kp_id)
{
    $colspan = PenentuanJangkaBenah::where('kp_id',$kp_id)->get();
    return $colspan;
}

function getjumlahrow($daur,$kp_id)
{
    $jmlkolom = PenentuanJangkaBenah::where('kp_id',$kp_id)->count();
    return $jmlkolom;
}

function getNha($jmlpohon,$luaspu)
{
    if($luaspu == 0.02)
    {
        $luas = 50;
    }elseif($luaspu == 0.04){
        $luas = 25;
    }elseif($luaspu == 0.10){
        $luas = 10;
    }elseif($luaspu == 0){
        $luas = 0;
    }
    $nlap = $jmlpohon*$luas;
    if($nlap){
    return $nlap;
    }
    return 0;
}

function getHl($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getHllalu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKps($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKpsLalu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getHas($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getHasLalu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getTbp($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getTbpLalu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKpkh($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKpkhLalu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getProduktifLalu($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKu2($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKu3($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKu4($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getLdti($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getWw($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getHtkh($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKtn($kpId,$tahun,$kelashutan)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getLuasKawasan($kpId,$tahun)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getPersen($kpId,$tahun,$kelashutan)
{
    $lalu = 0;
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    $lalu = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan',$kelashutan)->sum('luas_penetapan');
    if($data > 0 && $lalu > 0)
    {

        $persen = isset($lalu) ? ($data/$lalu)*100 : 0;
        // $persen = isset($lalu)  ? ($data/$lalu)*100 : 0;
        return number_format($persen,2,',','.');
    }
    return 0.00;
}

function getPersenHp($kpId,$tahun)
{
    $lalu = 0;
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->where('kelas_hutan','<>',2)->sum('luas_penetapan');
    $lalu = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->where('kelas_hutan','<>',2)->sum('luas_penetapan');
    if($data > 0 && $lalu >0)
    {
        $persen = $lalu == 0 ? 0 : $data/$lalu*100;
        return number_format($persen,2,',','.');
    }
    return 0;
}

function getPersenTotal($kpId,$tahun)
{
    $lalu = 0;
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun)->sum('luas_penetapan');
    $lalu = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->sum('luas_penetapan');
    if($data)
    {
        // $lalu == 0 ? 0 : $data/$lalu*100;
        $persen = $lalu == 0 ? 0 : $data/$lalu*100;
        return number_format($persen,2,',','.');
    }
    return 0;
}

function getLuasKawasanLalu($kpId,$tahun)
{
    $data = HasilRisalah::where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getLuasHp($kpId,$tahun)
{
    $data = HasilRisalah::where('kelas_hutan','<>',2)->where('kp_id',$kpId)->where('tahun_risalah',$tahun)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getLuasHpLalu($kpId,$tahun)
{
    $data = HasilRisalah::where('kelas_hutan','<>',2)->where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getVolumeUtr($umur,$bonita,$jenis)
{
    $volume = DB::table('sr_standingstock')
    ->where('umur',$umur)
    ->where('bonita',$bonita)
    ->where('tanaman_id',$jenis)->first();
    if($volume){
        return $volume->vol;
    }
    return 0;
}

function getKawasanPerlindunganLalu($kpId,$tahun)
{
    $data = HasilRisalah::whereIn('kelas_hutan',[4,28,1,5])->where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKawasanPerlindungan($kpId,$tahun)
{
    $data = HasilRisalah::whereIn('kelas_hutan',[4,28,1,5])->where('kp_id',$kpId)->where('tahun_risalah',$tahun)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function formatIndonesia($number)
{
    return number_format($number,2,',','.');
}

function getpenggunaanlainlalu($kpId,$tahun)
{
    $data = HasilRisalah::whereIn('kelas_hutan',[6,3,35,23])->where('kp_id',$kpId)->where('tahun_risalah',$tahun-10)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getpenggunaanlainsekarang($kpId,$tahun)
{
    $data = HasilRisalah::whereIn('kelas_hutan',[6,3,35,23])->where('kp_id',$kpId)->where('tahun_risalah',$tahun)->sum('luas_penetapan');
    if($data)
    {
        return number_format($data,2,',','.');
    }
    return 0;
}

function getKelasHutan($kelasP,$jnsTan,$kbd,$dkn,$umur,$idTan)
{
    $potensi = array('ACC. MANGIUM','DAMAR','JATI','ARET','KAYU PUTIH','KESAMBI','MAHONI','MANGROVE','PINUS','SENGON');
    $kelasumur = KelasUmur::with('kelashutan')->where('jenistanaman_id',$idTan)->where('umur',$umur)->first();
    if($jnsTan){
        if($kbd >= 0.60 && $kelasP == $jnsTan && $umur < 80 || $dkn >= 0.5 && $kelasP == $jnsTan && $umur < 40 ){
            if(isset($kelasumur)){
                $kelashutan = $kelasumur->kelashutan->id;
            }
        }elseif($kbd >= 0.60 && $umur >= 80 && $kelasP == $jnsTan){
            $kelashutan = 25;
        }elseif($kbd >= 0.30 && $kbd <= 0.59 && $umur > 41 && $kelasP == $jnsTan){
            $kelashutan = 26;
        }elseif($kbd >= 0.06 && $kbd <= 0.59 && $kelasP == $jnsTan){
            $kelashutan = 27;
        }elseif($dkn >= 0.5 && $kelasP != $jnsTan  && in_array($jnsTan,$potensi) || $kbd >= 0.3 && $kelasP != $jnsTan  && in_array($jnsTan,$potensi) ){
            #TJKL
            $kelashutan = 29;
        }elseif($dkn >= 0.5 && $kelasP != $jnsTan  && !in_array($jnsTan,$potensi) || $kbd >= 0.30 && $kelasP != $jnsTan){
            #TKL
            $kelashutan = 32;
        }elseif($kbd < 0.3 && $kelasP == $jnsTan){
            $kelashutan = 36;
        }elseif($kbd < 0.05 && $dkn < 0.3){
            $kelashutan = 31;
        }elseif($kbd < 0.30 && $kelasP != $jnsTan && in_array($jnsTan,$potensi)){
            $kelashutan = 30;
        }elseif($kbd <= 0.29 && $umur > 16 && !in_array($jnsTan,$potensi) ){
            $kelashutan = 33;
        }elseif($kbd <= 0.29 && $umur > 16 ){
            $kelashutan = 27;
        }
        if(isset($kelashutan)){
            return $kelashutan;
        }
        return '';
    }
    return 31;
}

function getNtabel($jenis,$umur,$bonita,$jarak)
{
    // return $jenis;
    if($jenis == 30){
        $ntabel = NNormal::whereumur($umur)->whereid_jaraktanam($jarak)->where('jenistanaman_id',$jenis)->first();
    }else{
        $ntabel = TabelJenis::where('jenistanaman_id',$jenis)->where('umur',$umur)->where('bonita',$bonita)->first();
    }
    if($ntabel){
        return $ntabel->ntabel;
    }
    return 0;
}

function getLbds($jenis,$umur,$bonita,$jarak)
{
    // return $jenis;
    if($jenis == 30){
        $ntabel = NNormal::whereumur($umur)->whereid_jaraktanam($jarak)->where('jenistanaman_id',$jenis)->first();
    }else{
        $ntabel = TabelJenis::where('jenistanaman_id',$jenis)->where('umur',$umur)->where('bonita',$bonita)->first();
    }
    if($ntabel){
        return $ntabel->lbds;
    }
    return 0;
}

function getTahunTanam($anakpetak_id)
{
    $data = Buatts::where('anakpetak_id', $anakpetak_id)->average('tahuntanam');
    return $data;
}

function getNamaAsper($bkphId)
{
    $bkph = Bkph::where('kode',$bkphId)->first();
    $asper = MasterEmployee::where('position_code','0324')->where('is_active',1)->where('department_code',$bkph->department_code)->first();
    if($asper){
    return $asper->nama;
    }
    return '';
}

function getEmpCodeAsper($bkphId)
{
    $bkph = Bkph::where('kode',$bkphId)->first();
    $asper = MasterEmployee::where('position_code','0324')->where('is_active',1)->where('department_code',$bkph->department_code)->first();
    if($asper){
    return $asper->emp_code;
    }
    return '';
}

function getKrph($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $krph = MasterEmployee::where('position_code','0327')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($krph){
    return $krph;
    }
    return '';
}

function getNamaKrph($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $krph = MasterEmployee::where('position_code','0327')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($krph){
        return $krph->nama;
    }
    return '';
}

function getDepartemen($bkph_id)
{
    $asper = Bkph::where('kode',$bkph_id)->first();
    // return $asper;
    $data = MasterEmployee::where('department_code',$asper->department_code)
                            ->where('is_active',1)
                            ->pluck('emp_code')->toArray();
    return $data;
}

function getSite($kph_id)
{
    $kph = Kph::where('kode',$kph_id)->first();
    // return $asper;
    $data = MasterEmployee::where('site_code',$kph->site_code)
                        ->where('is_active',1)
                        ->pluck('emp_code')->toArray();
    return $data;
}

function getPhw($kphw_id)
{
    $kph = Kph::where('kphw_id',$kphw_id)->pluck('kode')->toArray();
    // return $asper;
    $data = MasterEmployee::whereIn('site_code',$kph)
    ->where('is_active',1)
    ->pluck('emp_code')->toArray();
    return $data;
}

function getDivisi($unitkerja_id)
{
    $kph = Kph::where('unit_kerja_id',$unitkerja_id)->pluck('kode')->toArray();
    // return $asper;
    $data = MasterEmployee::whereIn('site_code',$kph)
    ->where('is_active',1)
    ->pluck('emp_code')->toArray();
    return $data;
}

function getKaryawan($empcode)
{
    $data = MasterEmployee::where('emp_code',$empcode)->where('is_active',1)->first();
    return $data->position_code.'|'.$data->site_code.'|'.$data->section_code;
}

function getEmpCodeKrph($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $krph = MasterEmployee::where('position_code','0327')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($krph){
        return $krph->emp_code;
    }
    return '';
}

function getNpkKrph($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $krph = MasterEmployee::where('position_code','0327')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($krph){
        return $krph->npk;
    }
    return '';
}

function getNamaMandorTebang($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0633')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($mandor){
        return $mandor->nama;
    }
    return '';
}

function getMandorTebang($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0633')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($mandor){
        return $mandor;
    }
    return '';
}

function getEmpCodeMandorTebang($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0633')->where('is_active',1)->where('section_code',$rph->section_code)->first();
    if($mandor){
        return $mandor->emp_code;
    }
    return '';
}

function getEmpCodeMandorTanam($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0632')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor->emp_code;
    }
    return '';
}

function getEmpCodeMandorSemai($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0627')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor->emp_code;
    }
    return '';
}

//Mandor Polter 0629
function getMandorPolter($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0629')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor;
    }
    return '';
}

//Mandor Sadap 0630
function getEmpCodeMandorSadap($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0630')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor->emp_code;
    }
    return '';
}

function getMandorSadap($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0630')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor;
    }
    return '';
}

//Mandor Persemaian 0627
function getMandorPersemaian($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0627')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor;
    }
    return '';
}

//Mandor Wsata 0637
function getMandorWisata($rphId)
{
    $rph = Rph::where('kode',$rphId)->first();
    $mandor = MasterEmployee::where('position_code','0637')->where('section_code',$rph->section_code)->where('is_active',1)->first();
    if($mandor){
        return $mandor;
    }
    return '';
}

function getAsper($bkphId)
{
    $bkph = Bkph::where('kode',$bkphId)->first();
    $name = str_replace("BKPH ","",$bkph->name);
    $asper = MasterEmployee::where('position_code','0324')
    ->where('is_active',1)
    ->where('department_code',$bkph->department_code)->first();
    if($asper){
        return $asper;
    }
    return '';
}

function getNpkAsper($bkphId)
{
    $bkph = Bkph::where('kode',$bkphId)->first();
    $name = str_replace("BKPH ","",$bkph->name);
    $asper = MasterEmployee::where('position_code','0324')
    ->where('is_active',1)
    ->where('department_code',$bkph->department_code)->first();
    if($asper){
        return $asper->npk;
    }
    return 'PHT';
}

function getAdm($kphId)
{
    $kph = Kph::where('kode',$kphId)->first();
    $adm = MasterEmployee::whereIn('position_code',['0308','0339','0341'])
    ->where('site_code',$kph->site_code)
    ->where('is_active',1)
    ->first();
    if($adm){
        return $adm;
    }
    return '';
}

function getNamaAdm($kphId)
{
    $kph = Kph::where('kode',$kphId)->first();
    $adm = MasterEmployee::whereIn('position_code',['0308','0339','0341'])
    ->where('site_code',$kph->site_code)
    ->where('is_active',1)
    ->first();
    if($adm){
    return $adm->nama;
    }
    return '';
}

function getEmpCodeAdm($kphId)
{
    $kph = Kph::where('kode',$kphId)->first();
    $adm = MasterEmployee::whereIn('position_code',['0308','0339','0341'])
    ->where('site_code',$kph->site_code)
    ->where('is_active',1)
    ->first();
    if($adm){
    return $adm->emp_code;
    }
    return '';
}

function getNpkAdm($kphId)
{
    $kph = Kph::where('kode',$kphId)->first();
    $adm = MasterEmployee::whereIn('position_code',['0308','0339','0341'])
    ->where('site_code',$kph->site_code)
    ->where('is_active',1)
    ->first();
    if($adm){
        return $adm->npk;
    }
    return 'PHT';
}

function getJabatanAdm($kphId)
{
    $kph = Kph::where('kode',$kphId)->first();
    $adm = MasterEmployee::whereIn('position_code',['0308','0339','0341'])
    ->where('site_code',$kph->site_code)
    ->where('is_active',1)
    ->first();
    if($adm){
        return $adm->position;
    }
    return '';
}

function getJenis($jenis)
{
    // return $jenis;
    if(isset($jenis)){
        $position = Auth::user()->position;
        $data = Kelasperusahaan::where('name',$jenis);
        if(isset($position->kph_id)){
            $bh = Bagianhutan::where('kph_id',$position->kph_id)->pluck('kode')->toArray();
            $data->whereIn('bh_id',$bh);
        }elseif(isset($position->kphw_id)){
            $kph = Kph::where('kphw_id',$position->kphw_id)->pluck('kode')->toArray();
            $bh = Bagianhutan::where('kph_id',$kph)->pluck('kode')->toArray();
            $data->whereIn('bh_id',$bh);
        }elseif(isset($position->unit_kerja_id)){
            $kph = Kph::where('unit_kerja_id',$position->unit_kerja_id)->pluck('kode')->toArray();
            $bh = Bagianhutan::where('kph_id',$kph)->pluck('kode')->toArray();
            $data->whereIn('bh_id',$bh);
        }

        return $data->pluck('kode')->toArray();
    }
    return 0;
}

function getWorkOrderIn($empcode,$tahun)
{
    $data = Workorder::where('tahun',$tahun)->where('status','Open')->where('untuk_id',$empcode)->count();
    if($data){
        return $data;
    }
    return 0;
}

function getWorkOrderOn($empcode,$tahun)
{
    $data = Workorder::where('tahun',$tahun)->where('status','In Progress')->where('untuk_id',$empcode)->count();
    if($data){
        return $data;
    }
    return 0;
}

function getWorkOrderFinish($empcode,$tahun)
{
    $data = Workorder::where('tahun',$tahun)->where('status','Complete')->where('untuk_id',$empcode)->count();
    if($data){
        return $data;
    }
    return 0;
}

function formatnumber($number)
{
    return number_format($number, 2, ",", ".");
}

function getKelasperusahaan($jenis)
{
    // return $jenis;
    $position = Auth::user()->position;
    // return $position;
    if(isset($jenis)){
        $data = Kelasperusahaan::where('name',$jenis);
        // return $data->get();
        if($position->kph_id > 0){
            $bh = Bagianhutan::where('kph_id',$position->kph_id)->pluck('kode')->toArray();
            // return $bh;
            $data->whereIn('bh_id',$bh);
        }elseif($position->kphw_id > 0){
            $kph = Kph::where('kphw_id',$position->kphw_id)->pluck('kode')->toArray();
            $bh = Bagianhutan::whereIn('kph_id',$kph)->pluck('kode')->toArray();
            // return $bh;
            $data->whereIn('bh_id',$bh);
        }elseif($position->unit_kerja_id > 0){
            $kph = Kph::where('unit_kerja_id',$position->unit_kerja_id)->pluck('kode')->toArray();
            $bh = Bagianhutan::whereIn('kph_id',$kph)->pluck('kode')->toArray();
            // return $bh;
            $data->whereIn('bh_id',$bh);
        }elseif(isset($position->company_id)){
            $kph = Kph::pluck('kode')->toArray();
            $bh = Bagianhutan::whereIn('kph_id',$kph)->pluck('kode')->toArray();
            // return $bh;
            $data->whereIn('bh_id',$bh);
        }
        return $data->pluck('kode')->toArray();
    }
    return 0;
}

function getJenisByPosition($position)
{
    // return $position;
    if($position->rph_id > 0){
        $bh = Bagianhutan::where('kph_id',$position->kph_id)->pluck('kode')->toArray();
        $data = Kelasperusahaan::whereIn('bh_id',$bh)->select('name')->groupBy('name')->get();
    }elseif($position->bkph_id > 0){
        $bh = Bagianhutan::where('kph_id',$position->kph_id)->pluck('kode')->toArray();
        $data = Kelasperusahaan::whereIn('bh_id',$bh)->select('name')->groupBy('name')->get();
    }elseif($position->kph_id > 0){
        $bh = Bagianhutan::where('kph_id',$position->kph_id)->pluck('kode')->toArray();
        $data = Kelasperusahaan::whereIn('bh_id',$bh)->select('name')->groupBy('name')->get();
    }elseif($position->kphw_id > 0){
        $kph = Kph::whereIn('kphw_id',[$position->kphw_id])->pluck('kode')->toArray();
        $bh = Bagianhutan::whereIn('kph_id',$kph)->pluck('kode')->toArray();
        $data = Kelasperusahaan::whereIn('bh_id',$bh)->select('name')->groupBy('name')->get();
    }elseif($position->unit_kerja_id >0){
        $kph = Kph::whereIn('unit_kerja_id',[$position->unit_kerja_id])->pluck('kode')->toArray();
        $bh = Bagianhutan::whereIn('kph_id',$kph)->pluck('kode')->toArray();
        $data = Kelasperusahaan::whereIn('bh_id',$bh)->select('name')->groupBy('name')->get();
    }elseif($position->company_id > 0){
        $data = Kelasperusahaan::select('name')->groupBy('name')->get();
    }
    return $data;
}

function getWorkorder()
{
    $position = Auth::user()->employee;
    if(isset(Auth::user()->position->rph_id)){
        $untuk = MasterEmployee::where('section_code', $position->section_code)
        ->where('is_active',1)
        ->get();
    }elseif(isset(Auth::user()->position->bkph_id)){
        $untuk = MasterEmployee::where('department_code', $position->department_code)
        ->where('is_active',1)
        ->get();
    }elseif(isset(Auth::user()->position->kph_id)){
        $untuk = MasterEmployee::where('site_code', $position->site_code)
        ->where('is_active',1)
        ->get();
    }elseif(isset(Auth::user()->position->unit_kerja_id)){
        $untuk = MasterEmployee::where('division_code', $position->division_code)
        ->where('is_active',1)
        ->get();
    }
    if($untuk){
        return $untuk;
    }
    return [];
}

function getJudul()
{
    $position = Auth::user()->position;
    if(isset($position->rph_id)){
        return 'Anak Petak';
    }elseif(isset($position->bkph_id)){
        return 'RPH';
    }elseif(isset($position->kph_id)){
        return 'BKPH';
    }elseif(isset($position->kphw_id)){
        return 'KPH';
    }elseif(isset($position->unit_kerja_id)){
        return 'KPH';
    }elseif(isset($position->company_id)){
        return 'KPH';
    }
}

function getNotifRttTebangan($tahun){
    // return 0;
    $position = Auth::user()->position;

    $data = RttTebangan::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->select(DB::raw('bkph_id'))
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->select(DB::raw('kph_id'))
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->select(DB::raw('kph_id'))
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }
    // return $data->get();
    if(count($data->get()) > 0){
        return count($data->get());
    }
    return 0;
}

function getNotifRttPersemaian($tahun){
    $position = Auth::user()->position;

    $data = RttPersemaian::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->select(DB::raw('bkph_id as jumlah'))
            ->where('kph_id', $position->kph_id)
            ->where('status', 0)
            ->groupBy('bkph_id','jenis_pokok','jenis_pengisi');
    }elseif(isset($position->kphw_id)){
        $data->select(DB::raw('kph_id'))
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id','jenis_pokok','jenis_pengisi');
    }elseif(isset($position->unit_kerja_id)){
        $data->select(DB::raw('kph_id'))
            ->where('unit_kerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id','jenis_pokok','jenis_pengisi');
    }

    return count($data->get());
}

function getNotifRttPersiapanTanaman($tahun){
    $position = Auth::user()->position;

    $data = RttPersiapanTanaman::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->select(DB::raw('bkph_id as jumlah'))
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->select(DB::raw('kph_id as jumlah'))
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->select(DB::raw('kph_id as jumlah'))
            ->where('unit_kerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }

    return count($data->get());
}

function getNotifRttTanaman($tahun){
    $position = Auth::user()->position;

    $data = RttTanaman::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('jenis_reboisasi','bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('jenis_reboisasi','kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('jenis_reboisasi','kph_id');
    }

    return count($data->get());
}

function getNotifRttPemeliharaan1($tahun){
    $position = Auth::user()->position;

    $data = Rtt_Pemel1::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }

    return count($data->get());
}

function getNotifRttPemeliharaan2($tahun){
    $position = Auth::user()->position;

    $data = Rtt_Pemel2::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }

    return count($data->get());
}

function getNotifRttPemeliharaan4($tahun){
    $position = Auth::user()->position;

    $data = Rtt_Pemel4::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }
    if($data){
        return count($data->get());
        }
        return 0;
}

function getNotifRttPemeliharaan5($tahun){
    $position = Auth::user()->position;

    $data = Rtt_Pemel5::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }
    if($data){
        return count($data->get());
        }
        return 0;
}

function getNotifRttPerawatan($tahun){
    $position = Auth::user()->position;

    $data = RttPerawatan::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }

    if($data){
        return count($data->get());
        }
        return 0;
}

function getNotifRttSadapan($tahun){
    $position = Auth::user()->position;

    $data = RttSadapan::where('tahun', $tahun);

    if(isset($position->kph_id)){
        $data->selectRaw('bkph_id')
            ->where('kph_id', $position->kph_id)->where('status', 0)
            ->groupBy('bkph_id');
    }elseif(isset($position->kphw_id)){
        $data->selectRaw('kph_id')
            ->where('kphw_id',$position->kphw_id)->where('status', 2)
            ->groupBy('kph_id');
    }elseif(isset($position->unit_kerja_id)){
        $data->selectRaw('kph_id')
            ->where('unitkerja_id',$position->unit_kerja_id)->where('status', 3)
            ->groupBy('kph_id');
    }
    if($data){
        return count($data->get());
        }
        return 0;
}

function getAngka($text)
{
    $kode = strtoupper($text);
    $string = preg_replace("/[^A-Z]/","",$kode);
    $posisi = strpos($kode,$string);
    $anakpetak = substr($kode,$posisi);
    $cucu = substr($kode,$posisi+1);
    $cucu = preg_replace("/[^0-9]/","",$cucu);
    $anak = str_replace(
        ['-','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
        ['','001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020','021','022','023','024','025','026'],
        $string);
    $cucupetak='';
    if($cucu){
        $cucupetak='000';
        if(strlen($cucu) ==1){
            $cucupetak = '00'.$cucu;
        }elseif(strlen($cucu) ==2){
            $cucupetak = '0'.$cucu;
        }
    }
    $kodedata = $anak.$cucupetak;

    if(strlen($kodedata) == 3){
        $kode_id = $kodedata.'000';
    }else{
        $kode_id = $kodedata;
    }
    return substr($kode_id,0,9);
}

function qrcode($nama)
{
    $writer = new PngWriter();
    // Create QR code
    $qrCode = QrCode::create($nama)
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(300)
        ->setMargin(10)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));

    // Create generic logo
    $logo = Logo::create(public_path().'/img/PHTQRCode.png')
        ->setResizeToWidth(70)
        ->setPunchoutBackground(true)
    ;

    // Create generic label
    $label = Label::create('Ditanda tangani secara digital')
        ->setTextColor(new Color(0, 0, 0));

    $result = $writer->write($qrCode, $logo, $label);
    $dataUri = $result->getDataUri();
    echo $dataUri;
}

function getnotifikasi($emp_code,$spk)
{
    $firebaseToken = User::where('emp_code',$emp_code)->pluck('fcm_token')->toArray();
        // return $firebaseToken;
    $SERVER_API_KEY = 'AAAAVtIKN_s:APA91bGEzxymdkKHlI62LvIPt_suxpMjYkd2wrtWrlcDL47KBpYIvz_QmKRjRkI9nIFn8mgR_41OuQJ7og3lb-Lo5N3JcUQdZ1dUuLhu8PSjX9Rk2xLGN4tB6xQZgqgnhpdhFR7SytZS';
    $data = [
        "registration_ids" => $firebaseToken,
        "notification" => [
            "title" => 'Workoder Masuk',
            "body" => $spk,
        ]
    ];
    // return $data;
    $dataString = json_encode($data);
    $headers = [
        'Authorization: key=' . $SERVER_API_KEY,
        'Content-Type: application/json',
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
    $response = curl_exec($ch);
    // return $response;
    return back()->with('success', 'Notification send successfully.');
}
