<?php

use Hashids\Hashids;

class Number
{
    public static function number_format($number) {
        return number_format($number, 2, ",", ".");
    }
}