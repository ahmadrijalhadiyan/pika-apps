<?php
use App\Models\KonversiHuruf;
use App\Models\UnitKerja;
use App\Models\Kphw;
use App\Models\Kph;
use App\Models\Bkph;
use App\Models\Rph;
use App\Models\Kelasperusahaan;
use App\Models\UserPosition;
use App\User;

function getAnakpetak($petak, $anak) {
    $huruf = preg_replace("/[^a-zA-Z]+/", "", $anak);
    $cucu = '00';
    if(Str::length($huruf) == 1){
        $abjad1 = KonversiHuruf::where('nama_abjad',$huruf)->first();
        $kode = '0'.$abjad1->nama_angka;
    }elseif(Str::length($huruf) > 1){
        $abjad =str_split($huruf);
        $abjad1 = KonversiHuruf::where('nama_abjad',$abjad[0])->first();
        $abjad2 = KonversiHuruf::where('nama_abjad',$abjad[1])->first();
        $kode1 = $abjad1->nama_angka+$abjad2->nama_angka;
        if($kode1 > 9){
            $kode = '0'.$kode1.'0';
        }else{
            $kode = '00'.$kode1.'0';
        }
        
    }
    $cucuanak = explode('-',$anak);
    if(isset($cucuanak[1])){
        if(Str::length($cucuanak[1]) == 1){
            $cucu = '0'.$cucuanak[1];
        }else{
            $cucu = $cucuanak[1];
        }
    }
    
    $hasil = $petak.$kode.$cucu;
    
    return $hasil;
}

function getLokasi() {
    
    $posisi = Auth::user()->position;
    // return $posisi;
    if(isset($posisi->rph_id)){
        $lokasi = Rph::where('kode',$posisi->rph_id)->pluck('name')->first();
    }elseif(isset($posisi->bkph_id)){
        $lokasi = Bkph::where('kode',$posisi->bkph_id)->pluck('name')->first();
    }elseif(isset($posisi->kph_id)){
        $lokasi = Kph::where('kode',$posisi->kph_id)->pluck('name')->first();
    }elseif(isset($posisi->kphw_id)){
        $lokasi = Kphw::where('kode',$posisi->kphw_id)->pluck('name')->first();
    }elseif(isset($posisi->unit_kerja_id)){
        $lokasi = UnitKerja::where('kode',$posisi->unit_kerja_id)->pluck('name')->first();
    }elseif (isset($posisi->company_id)){
        $lokasi = "Perhutani";
    }
    return $lokasi;

}

function getByKPid($id)
{
    $data = Kelasperusahaan::with('bagianhutan.kph.kphw')->where('kode',$id)->first();
    if($data){
        return $data;
    }
    return 0;
}

function getkphw($id)
{
    $kp = getByKPid($id);
    $posisi = UserPosition::where('kphw_id',$kp->bagianhutan->kph->kphw_id)->pluck('user_id')->toarray();
    
    $user  = User::with(['roles' => function($query){
        $query->where('name', 'Kasi PHW');
    }])->whereIn('id',$posisi)->get();
    // return $user;
    foreach($user as $row){
        if(count($row->roles) > 0){
            $userId[] = $row->id;
        }
    }
    // return $userId;
    $jum = count($userId);
    for($i=0;$i<$jum; $i++)
    {
        $user_id = $userId[$i];
    }
    if($userId){
    return $user_id;
    }
    return 0;
}

function getWakaphw($id)
{
    $kp = getByKPid($id);
    $posisi = UserPosition::where('kphw_id',$kp->bagianhutan->kph->kphw_id)->pluck('user_id')->toarray();
    
    $user  = User::with(['roles' => function($query){
        $query->where('name', 'Waka PHW');
    }])->whereIn('id',$posisi)->get();
    // return $user;
    foreach($user as $row){
        if(count($row->roles) > 0){
            $userId[] = $row->id;
        }
    }
    // return $userId;
    $jum = count($userId);
    for($i=0;$i<$jum; $i++)
    {
        $user_id = $userId[$i];
    }
    if($userId){
    return $user_id;
    }
    return 0;
}