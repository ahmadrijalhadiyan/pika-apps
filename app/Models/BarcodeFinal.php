<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarcodeFinal extends Model
{
    use HasFactory;

    protected $table = 'barcode_final';

    public function lokasi()
    {
        return $this->belongsTo('App\Models\Lokasi', 'kode_lokasi','kode_lokasi');
    }

    public function lokasidkb()
    {
        return $this->belongsTo('App\Models\Lokasi', 'kode_lokasi_dkb','kode_lokasi');
    }

    public function lokasitpk()
    {
        return $this->belongsTo('App\Models\Lokasi', 'kode_tpk','kode_lokasi');
    }

    public function jenis()
    {
        return $this->belongsTo('App\Models\JenisKayu','kd_jns_ky','kd_jns_ky');
    }

    public function asal()
    {
        return $this->belongsTo('App\Models\AsalKayu','asal_ky','asal_ky');
    }

    public function dati()
    {
        return $this->belongsTo('App\Models\Kabupaten', 'kode_dt2','kode_dt2');
    }
}
