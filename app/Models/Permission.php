<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;
use Spatie\Permission\Models\Permission as PermissionModel;
use App\Models\Module;

class Permission extends PermissionModel
{

    public function module()
    {
        return $this->belongsTo('App\Models\Module', 'module_id');
    }
}
