<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;
use App\Models\Permission;

class Module extends Model
{
    
    protected $table = 'modules';

    public function permissions()
    {
        return $this->hasMany('App\Models\Permission', 'module_id');
    }
}