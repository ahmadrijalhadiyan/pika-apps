<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisKayu extends Model
{
    use HasFactory;

    protected $table = 'r_global_jns_ky';
}
