<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propinsi extends Model
{
    use HasFactory;
    protected $table = 'r_global_propinsi';
    
    public function dati2()
    {
        return $this->belongsTo('App\Models\Kabupaten', 'kode_dt2','kode_dt2');
    }
}
