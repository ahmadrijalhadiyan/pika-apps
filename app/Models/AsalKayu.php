<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsalKayu extends Model
{
    use HasFactory;

    protected $table = 'r_global_asal_ky';
}
