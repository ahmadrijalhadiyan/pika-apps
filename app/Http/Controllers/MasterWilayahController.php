<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterWilayahController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:masterwilayah-list');
         $this->middleware('permission:masterwilayah-create', ['only' => ['create','store']]);
         $this->middleware('permission:masterwilayah-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:masterwilayah-delete', ['only' => ['destroy']]);
    }

    public function propinsi()
    {
        $title = 'Delete Data Provinsi!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $propinsi = DB::table('r_global_propinsi')
        ->LEFTJOIN('r_global_dati2', 'r_global_propinsi.kode_dt2', '=', 'r_global_dati2.kode_dt2' )
        ->select('r_global_dati2.kode_dt2','r_global_dati2.ket_30','r_global_propinsi.nama_propinsi')
        ->get();
        return view('configs.propinsi.index',compact('propinsi'));
    }

    public function propinsi_update(Request $r, $id)
    {
        $propinsi = DB::table('r_global_propinsi')->where('kode_dt2',$id)->update([
            'nama_propinsi' => $r->nama_propinsi,
        ]);
        return redirect()->back()->with('success','Data Provinsi Berhasil diedit');
    }

    public function propinsi_hapus($id)
    {
        $propinsi = DB::table('r_global_propinsi')->where('kode_dt2',$id)->delete();
        return redirect()->back()->with('success','Data Provinsi Berhasil dihapus');
    }

    public function kota(){
        $title = 'Delete Data Kota!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $kota = DB::table('r_global_kota')->get();
        return view('configs.kota.index',compact('kota'));
    }

    public function kota_update(Request $r, $id)
    {
        $kota = DB::table('r_global_kota')->where('kode_kota',$id)->update([
            'ket_kota' => $r->ket_kota,
        ]);
        return redirect()->back()->with('success','Data Kota Berhasil diedit');
    }

    public function kota_hapus($id)
    {
        $kota = DB::table('r_global_kota')->where('kode_kota',$id)->delete();
        return redirect()->back()->with('success','Data Kota Berhasil dihapus');
    }

    public function kabupaten(){
        $title = 'Delete Data Kabupaten!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $kabupaten = DB::table('r_global_dati2')->get();
        return view('configs.kabupaten.index',compact('kabupaten'));
    }

    public function kabupaten_update(Request $r, $id)
    {
        $kabupaten = DB::table('r_global_dati2')->where('kode_dt2',$id)->update([
            'ket_30' => $r->ket_30,
        ]);
        return redirect()->back()->with('success','Data Kabupaten Berhasil diedit');
    }

    public function kabupaten_hapus($id)
    {
        $kabupaten = DB::table('r_global_dati2')->where('kode_dt2',$id)->delete();
        return redirect()->back()->with('success','Data Kabupaten Berhasil dihapus');
    }
}
