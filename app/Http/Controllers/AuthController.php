<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class AuthController extends Controller
{
     public function login()
    {
        return view('auth.login');
    }
    public function postlogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:4',
             
        ], [
            'email.email' => 'Wajib Menggunakan Email (abc@xxxx).',
            'password.min' => 'Password Tidak Benar.',
            'password.required' => 'Password Tidak Boleh Kosong.'
        ]);

        $data_user = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')   
        );
        //dd($data_user);
        //$tampilan_data = Auth::user()->is_active;
        //return $tampilan_data;

        if (Auth::attempt($data_user) && Auth::user()->is_active == 1 ) {
            return redirect('home');
        }elseif (Auth::attempt($data_user) && Auth::user()->is_active == 0 ) {
            return redirect('/login')->with('warning', 'Akun Anda Belum di Aktifkan Oleh Pengelola Sistem'); 
        } 
        else {
            $request->session()->flash('error', 'Username / Password Anda Salah');
            return redirect('/login');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
    
}
