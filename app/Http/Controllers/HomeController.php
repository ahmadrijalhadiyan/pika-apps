<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarcodeFinal;
use App\Models\User;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kayu = BarcodeFinal::with('jenis')->select(
            DB::raw('SUM(volume) as total_volume'),
            DB::raw('kd_jns_ky')
        )
        ->where('kode_thn',23)
        ->groupBy('kd_jns_ky')
        ->get();

        $pak[] = ['Sortimen','Volume'];
        foreach ($kayu as $key => $value) {
            $pak[++$key] = [$value->jenis->ket_30, (int)$value->total_volume];
            // $result[++$key] = ["Unit Kerja", $value->unitkerja->name];
        }

        $bkph = BarcodeFinal::with('jenis')->select(
            DB::raw('SUM(volume) as total_volume'),
            DB::raw('kode_lokasi_dkb')
        )
        ->where('flag_barcode', 'Y')
        ->groupBy('kode_lokasi_dkb')
        ->get();

        $bkph1[] = ['BKPH','Volume'];
        foreach ($bkph as $key => $value) {
            $bkph1[++$key] = [$value->lokasidkb->nama_lokasi, (int)$value->total_volume];
            // $result[++$key] = ["Unit Kerja", $value->unitkerja->name];
        }
        return view('home',compact('bkph1','pak'));
    }

    public function totalpengguna()
    {
        $data = User::count();
        return $data;
    }

    public function totaljeniskayu()
    {
        $data = BarcodeFinal::select('kd_jns_ky')->groupBy('kd_jns_ky')->get()->count();
        return $data;
    }

    public function totalasalkayu()
    {
        $data = BarcodeFinal::select('asal_ky')->groupBy('asal_ky')->get()->count();
        return $data;
    }

    public function totaljeniscacad()
    {
        $data = BarcodeFinal::select('cacad_ky')->groupBy('cacad_ky')->get()->count();
        return $data;
    }
}
