<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarcodeFinal;

class ScanController extends Controller
{
    public function index(){
        return view ('admin.pindai.index');
    }

    public function show(Request $r)
    {
        $data = BarcodeFinal::with('lokasi','jenis','lokasitpk','asal','dati')->where('barcode',$r->kode)->first();
        if($data){
            return response()->json([
                'status' => 'true',
                'message' => 'Data Ditemukan',
                'data' => $data
            ]);
        }
        return response()->json([
            'status' => 'false',
            'message' => 'Maaf, Data Barcode Tidak Ditemukan ',
            'data' => '',
        ]);
    }
}
