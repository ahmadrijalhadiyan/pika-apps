<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterKayuController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:masterkayu-list');
         $this->middleware('permission:masterkayu-create', ['only' => ['create','store']]);
         $this->middleware('permission:masterkayu-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:masterkayu-delete', ['only' => ['destroy']]);
    }
    public function asal_kayu(){
        $title = 'Delete Data!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $asl_kayu = DB::table('r_global_asal_ky')->get();
        return view('admin.asal_kayu.index',compact('asl_kayu'));
    }

    public function asal_kayu_update(Request $r)
    {
        //return $r->ket_4;
        $data = DB::table('r_global_asal_ky')->where('asal_ky',$r->asal_ky)
        ->update([
            'ket_10' => $r->ket_10,
            'ket_4' => $r->ket_4,
        ]);
            return redirect()->back()->with('success','Data Asal Kayu '.$r->ket_10.' Berhasil di Update');

    }

    public function asal_kayu_hapus($id)
    {
        $data = DB::table('r_global_asal_ky')->where('asal_ky',$id)->delete();
        return redirect()->back()->with('success','Data berhasil dihapus');
    }

    public function cacad_kayu(){
        $title = 'Delete Data!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $ccd_kayu = DB::table('r_global_cacad_ky')->get();
        return view('admin.cacad_kayu.index', compact('ccd_kayu'));
    }

    public function cacad_kayu_update(Request $r)
    {
        //return $r->ket_4;
        $data = DB::table('r_global_cacad_ky')->where('cacad_ky',$r->asal_ky)
        ->update([
            'ket_15' => $r->ket_15,
            'ket_4' => $r->ket_4,
        ]);
            return redirect()->back()->with('success','Data Cacad Kayu '.$r->ket_15.' Berhasil di Update');

    }

    public function cacad_kayu_hapus($id)
    {
        $data = DB::table('r_global_cacad_ky')->where('cacad_ky',$id)->delete();
        return redirect()->back()->with('success','Data berhasil dihapus');
    }

    public function jenis_kayu()
    {
        $title = 'Delete Data Jenis Kayu!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $jns_kayu = DB::table('r_global_jns_ky')->get();
        return view('admin.jenis_kayu.index', compact('jns_kayu'));
    }

    public function jenis_kayu_update(Request $r)
    {
        //return $r->ket_4;
        $data = DB::table('r_global_jns_ky')->where('kd_jns_ky',$r->kd_jns_ky)
        ->update([
            'ket_30' => $r->ket_30,
            'nama_latin' => $r->nama_latin_4,
        ]);
            return redirect()->back()->with('success','Data Jenis Kayu '.$r->ket_30.' Berhasil di Update');

    }

    public function jenis_kayu_hapus($id)
    {
        $data = DB::table('r_global_jns_ky')->where('kd_jns_ky',$id)->delete();
        return redirect()->back()->with('success','Data berhasil dihapus');
    }
}
