<?php
namespace App\Http\Controllers\Configs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Models\SiteSetting;
use DB, Datatables, Hasher, Validator, Auth, Artisan, Hash;

class SiteController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:site-settings-form');
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $setting = SiteSetting::all();
        // return $setting;
        $settingDefault = [];
        foreach ($setting as $row) {
            $settingDefault[$row->string] = $row->value;
        }
        return view('configs.site.index', compact('settingDefault'));
    }

    public function postIndex(Request $request)
    {
        $rules = [
            'sandi' => 'required_if:maintenance,==,on|confirmed',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else {
            DB::beginTransaction();

            try {
                if ($request->maintenance == 'on') {
                    $maintenance_mode = 1;
                }else {
                    $maintenance_mode = 0;
                }

                $settingMaintenanceMode = SiteSetting::where('string', 'maintenance_mode')->first();
                if (!isset($settingMaintenanceMode)) {
                    $settingMaintenanceMode = new SiteSetting();
                    $settingMaintenanceMode->string = 'maintenance_mode';
                }
                $settingMaintenanceMode->value = $maintenance_mode;
                if ($settingMaintenanceMode->save()) {
                    // set password recovery
                    $settingMaintenancePassword = SiteSetting::where('string', 'maintenance_password')->first();
                    if (!isset($settingMaintenancePassword)) {
                        $settingMaintenancePassword = new SiteSetting();
                        $settingMaintenancePassword->string = 'maintenance_password';
                    }
                    $settingMaintenancePassword->value = Hash::make($request->sandi);

                    if (!$settingMaintenancePassword->save()) {
                        if ($request->maintenance == 'on') {
                            $maintenance_mode = 0;
                        }else {
                            $maintenance_mode = 1;
                        }
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                return $e->getMessage();
                return redirect()->back()->with('error', 'Gagal menyimpan data setting')->withInput();
            }

            DB::commit();

            if ($maintenance_mode == 1) {
                Artisan::call('down');
            }else {
                Artisan::call('up');
            }
            

            return redirect('config/site',compact('maintenance_mode'))->with('success', 'Berhasil menyimpan setting');
        }
    }
}