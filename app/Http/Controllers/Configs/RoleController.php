<?php
namespace App\Http\Controllers\Configs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use DB, Datatables, Hasher, Auth;

class RoleController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:role-list');
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }


    public function index(Request $request)
    {
        return view('configs.role.index');
    }

    public function ajaxList(Request $request)
    {
        $data = Role::select([
            'id',
            'name',
            'guard_name',
        ])->orderBy('created_at', 'DESC');

        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = Hasher::encode($row->id);
                return "
                <a class=\"btn btn-sm btn-info\" href=\"". url('config/role/show/'.$hashed_id) ."\"><i class=\"fa fa-eye\"></i> Detail</a>
                <a class=\"btn btn-sm btn-primary\" href=\"". url('config/role/edit/'.$hashed_id) ."\"><i class=\"fa fa-edit\"></i> Ubah</a>
                <a class=\"btn btn-sm btn-danger delete-btn\" href=\"#\" data-id=\"". $hashed_id ."\" data-nama=\"". $row->name ."\"><i class=\"fa fa-trash\"></i> Hapus</a>
                ";
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        $permission = Permission::orderBy('name', 'ASC')->get();
        return view('configs.role.create',compact('permission'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->input('nama')]);
        $role->syncPermissions($request->input('permission'));

        return redirect('config/role')->with('success','Berhasil menginput data Role '.$role->name.'');
    }

    public function show($id)
    {
        $role = Role::find(Hasher::decode($id));
        $hashed_id = Hasher::encode($role->id);

        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",Hasher::decode($id))
            ->get();

        return view('configs.role.show',compact('role','rolePermissions', 'hashed_id'));
    }

    public function ajaxUser(Request $request, $id)
    {
        $data = User::whereHas('roles', function ($q) use ($id) {
            $q->where('id', Hasher::decode($id));
        })->orderBy('created_at', 'DESC');

        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = Hasher::encode($row->id);
                return "
                <a class=\"btn btn-sm btn-warning delete-btn\" href=\"#\" data-id=\"". $hashed_id ."\" data-nama=\"". $row->username ."\"><i class=\"fa fa-trash\"></i> Hapus</a>
                ";
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function getAddUser($id) {
        $role = Role::find(Hasher::decode($id));
        $users = User::all();

        if (isset($role)) {
            return view('configs.role.addUser', compact('role', 'users'));
        }
        return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }

    public function postAddUser(Request $request, $id) {
        $this->validate($request, [
            'anggota' => 'required|exists:users,id',
        ]);

        $hashed_id = $id;

        $user = User::find($request->anggota);
        $role = Role::find(Hasher::decode($id));

        if ($user->assignRole($role->name)) {
            return redirect('config/role/show/'.$hashed_id.'')->with('success','Berhasil menginput data Anggota '.$user->username.'');
        }
        return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }

    public function postDeleteUser(Request $request, $id) {
        // $data = Role::find($id);
        // return $data;
        // $user = Auth::user()->position;
        $delete = Role::where('id',Hasher::decode($id))->delete();
        // $role = Role::find(Hasher::decode($id));

        $hashed_id = $id;
        // if ((isset($user)) && ($user->removeRole($role->name))) {
            return redirect('config/role')->with('success','Berhasil menghapus data tipe user');
        // }

        // return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }

    public function edit($id)
    {
        $role = Role::find(Hasher::decode($id));
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",Hasher::decode($id))
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->toArray();

        return view('configs.role.edit',compact('role','permission','rolePermissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find(Hasher::decode($id));
        $role->name = $request->input('nama');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return redirect('config/role')->with('success','Berhasil mengubah data jenis user '.$role->name.'');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
                        ->with('success','Role deleted successfully');
    }
}
