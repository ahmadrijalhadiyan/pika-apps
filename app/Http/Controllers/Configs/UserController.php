<?php
namespace App\Http\Controllers\Configs;

use Illuminate\Http\Request;
// use Illuminate\Contracts\Filesystem\Filesystem;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use DB, Datatables, Hasher, Validator, Auth, Storage;

use Hash;

class UserController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:user-list',['only' => ['index']]);
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-update', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
         $this->middleware('permission:user-delete-role', ['only' => ['postDeleteRole']]);
    }

    public function index(Request $request)
    {
        $title = 'Delete User!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        return view('configs.users.index');
    }

    public function ajaxList(Request $request)
    {
        $position = Auth::user()->position;

        $data = User::with('roles')
            ->orderBy('created_at', 'DESC');

        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = Hasher::encode($row->id);
                return "
                <a class=\"btn btn-sm btn-info\" href=\"". url('config/user/show/'.$hashed_id) ."\"><i class=\"fa fa-eye\"></i> Detail</a>
                <a class=\"btn btn-sm btn-primary\" href=\"". url('config/user/edit/'.$hashed_id) ."\"><i class=\"fa fa-edit\"></i> Edit</a>
                <a class=\"btn btn-sm btn-danger\" href=\"".url('config/user/delete/'.$hashed_id)."\" data-confirm-delete=\"true\"><i class=\"fa fa-trash\"></i> Hapus</a>
                ";
            })
            ->addColumn('roles', function ($row) {
                $string = "";
                foreach($row->roles as $item) {
                    $string .= "<label class=\"badge badge-success\">".$item->name."</label> &nbsp;";
                }
                return $string;
            })
            ->addColumn('status', function($row){
                if($row->is_active == 1){
                    return "<div class=\"switch\">
                                <div class=\"onoffswitch\">
                                    <input type=\"checkbox\" data-id=\"".$row->id."\" checked=\"\" class=\"onoffswitch-checkbox status\" id=\"is_active".$row->id."\">
                                    <label class=\"onoffswitch-label\" for=\"is_active".$row->id."\">
                                        Aktif
                                    </label>
                                </div>
                            </div>";
                }else{
                    return "<div class=\"switch\">
                            <div class=\"onoffswitch\">
                                <input type=\"checkbox\" data-id=\"".$row->id."\" class=\"onoffswitch-checkbox status\" id=\"is_active".$row->id."\">
                                <label class=\"onoffswitch-label\" for=\"is_active".$row->id."\">
                                    Tidak Aktif
                                </label>
                            </div>
                        </div>";
                }
            })
            ->rawColumns(['action', 'roles','status'])
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        $user = Auth::user();

        if ($user->hasRole('Admin')) {
            $roles = Role::all();
        }else {
            $roles = Role::where('name', '!=', 'Admin')->orWhere('name', '!=', 'kaioken')->get();
        }
        // $karyawan = Karyawan::with('divisi','kph','bkph','rph')->where('status_karyawan',1)->get();
        return view('configs.users.create',compact('roles'));
    }

    public function store(Request $request)
    {
        //return $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'nullable|email|unique:users,email',
            'nik' => 'nullable|min:20',
            'password' => 'nullable|min:8',
            'nama_lengkap' => 'required',
            'roles' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else {
                $user = new User();
                $user->email = $request->email;
                $user->nik = $request->nik;
                $user->password = Hash::make($request->password);
                $user->name = $request->nama_lengkap;
                $user->is_active = 1;

                if ($user->save()) {
                    $user->assignRole($request->roles);
                }
            return redirect('config/user')->with('success', 'Berhasil menginput data pengguna '. $request->nama_lengkap .'');
        }
    }

    public function show($id)
    {
        $user = User::find(Hasher::decode($id));
        // return $user;
        if (isset($user)) {
            $hashed_id = Hasher::encode($user->id);
            return view('configs.users.show',compact('user', 'hashed_id'));
        }

        return redirect('config/user')->with('error', 'data tidak ditemukan')->withInput();
    }

    public function ajaxRole($id)
    {
        $data = Role::whereHas('users', function($q) use($id) {
            $q->where('id', Hasher::decode($id));
        });

        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = Hasher::encode($row->id);
                return "
                <a class=\"btn btn-sm btn-warning delete-btn\" href=\"#\" data-id=\"". $hashed_id ."\" data-nama=\"". $row->name ."\"><i class=\"fa fa-trash\"></i> Hapus</a>
                ";
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function postDeleteRole($id, $roleId)
    {
        $user = User::find(Hasher::decode($id));
        $role = Role::find(Hasher::decode($roleId));

        $hashed_id = $id;
        if ((isset($user)) && ($user->removeRole($role->name))) {
            return redirect('config/user/show/'.$hashed_id.'')->with('success','Berhasil menghapus data tipe user '.$user->name.'');
        }

        return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }

    public function edit($id)
    {
        $user = User::find(Hasher::decode($id));
        // return $user;
        if (isset($user)) {
            $roles = Role::all();
            $arrayRole = array();
            foreach($user->roles as $row) {
                $arrayRole[] = $row->id;
            }
            return view('configs.users.edit',compact('user','roles', 'arrayRole'));
        }

        return redirect('config/user')->with('error', 'data tidak ditemukan')->withInput();
    }


    public function update(Request $request, $id)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,'.Hasher::decode($id),
            'password' => 'nullable|min:8',
            'nik' => 'required',
            'nama_lengkap' => 'required',
            'roles' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else {
            $user = User::find(Hasher::decode($id));

            if (isset($user)) {
                $user->email = $request->email;
                $user->name = $request->nik;
                $user->name = $request->nama_lengkap;
                $user->password = Hash::make($request->password);
                $user->is_active = ($request->aktif) ? 1 : 0;

                if ($user->save()) {
                    DB::table('model_has_roles')->where('model_id',$user->id)->delete();
                    $user->assignRole($request->roles);
                    return redirect('config/user')->with('success', 'Berhasil mengubah Data User '. $request->nama_lengkap .'');
                }
                return redirect()->back()->with('error', 'Gagal Menginput Data User '. $request->nama_lengkap .'')->withInput();
            }
        }

        return redirect('config/user')->with('error', 'Data Tidak Ditemukan')->withInput();
    }


    public function destroy($id)
    {
        $authPosition = Auth::user()->position;
        $user = User::find(Hasher::decode($id));
        $user_name = $user->name;
        if((Auth::user()->id == Hasher::decode($id))) {
            return redirect('config/user')->with('error', 'Anda tidak dapat menghapus diri sendiri')->withInput();
        }elseif ((isset($user)) && ($user->delete())) {
            return redirect('config/user')->with('success','Berhasil menghapus user '. $user_name .'');
        }

        return redirect('config/user')->with('error', 'data tidak ditemukan')->withInput();
    }

    public function profile()
    {
        $user = User::find(Auth::user()->id);
        // return $user;
        return view('configs.users.profile',compact('user'));
    }

    public function postimage(Request $request)
    {
        // return $request->all();
        $request->validate([
            'image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        if($file = $request->hasFile('image')) {
            // $file = $request->file('image') ;
            $fileName = Auth::user()->username.'.'.$request->image->extension();
            $destinationPath = storage_path('app/public/img') ;
            $request->image->move($destinationPath,$fileName);
            $data = User::find(Auth::user()->id);
            $data->avatar = $fileName;
            $data->save();
            return redirect()->back()->with('success','Photo Profile berhasil disimpan') ;
        }else{
            return redirect()->back()->with('error', 'Gagal image melebihi kapasitas 2MB');
        }
    }

    public function updatepassword(Request $request)
    {
        // return $request->all();
        $data = User::find(Auth::user()->id);
        $data->email = $request->email;
        $data->telp = $request->telp;
        $data->password = Hash::make($request->password);
        if($data->save()){
            Auth::logout();
            return redirect('login');
        }
    }

    public function changeStatus(Request $request)
    {
        $user = User::find($request->user_id);
        $user->is_active = $request->status;
        $user->save();
        return response()->json(['success'=>'Status change successfully.']);

    }
}
