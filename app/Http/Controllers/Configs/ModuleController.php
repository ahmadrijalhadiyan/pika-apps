<?php
namespace App\Http\Controllers\Configs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Spatie\Permission\Models\Role;
// use Spatie\Permission\Models\Permission;
use App\Models\Module;
use App\Models\Permission;
use DB, Datatables, Hasher;


class ModuleController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:module-list');
         $this->middleware('permission:module-create', ['only' => ['getCreate','postCreate']]);
         $this->middleware('permission:module-update', ['only' => ['getUpdate','postUpdate']]);
         $this->middleware('permission:module-delete', ['only' => ['postDelete']]);
         $this->middleware('permission:module-create-permission', ['only' => ['getAddPermission','postAddPermission']]);
         $this->middleware('permission:module-delete-permission', ['only' => ['postDeletePermission']]);
    }

    public function index(Request $request)
    {
        return view('configs.modules.index');
    }

    public function ajaxList(Request $request)
    {
        $data = Module::select([
            'id',
            'name',
            'detail',
        ])->orderBy('created_at', 'DESC');

        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = Hasher::encode($row->id);
                return "
                <a class=\"btn btn-sm btn-info\" href=\"". url('config/module/show/'.$hashed_id) ."\"><i class=\"fa fa-eye\"></i> Detail</a>
                <a class=\"btn btn-sm btn-primary\" href=\"". url('config/module/edit/'.$hashed_id) ."\"><i class=\"fa fa-edit\"></i> Ubah</a>
                <a class=\"btn btn-sm btn-danger delete-btn\" href=\"#\" data-id=\"". $hashed_id ."\" data-nama=\"". $row->name ."\"><i class=\"fa fa-trash\"></i> Hapus</a>
                ";
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function getCreate()
    {
        return view('configs.modules.create');
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|unique:roles,name',
            'detail' => 'required|max:200',
        ]);

        $module = new Module();
        $module->name = $request->nama;
        $module->detail = $request->detail;
        if ($module->save()) {
            return redirect('config/module')->with('success','Berhasil menginput data Modul '.$request->name.'');
        }
        return redirect()->back()->with('error', 'Gagal menginput data Modul '. $request->name .'')->withInput();
    }

    public function show($id)
    {
        $data = Module::with('permissions')->find(Hasher::decode($id));
        $hashed_id = Hasher::encode($data->id);

        return view('configs.modules.show',compact('data', 'hashed_id'));
    }

    public function ajaxPermission(Request $request, $id)
    {
        $data = Permission::select([
            'id',
            'name',
            'guard_name',
        ])->where('module_id', Hasher::decode($id))->orderBy('created_at', 'DESC');

        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = Hasher::encode($row->id);
                return "
                <a class=\"btn btn-sm btn-warning delete-btn\" href=\"#\" data-id=\"". $hashed_id ."\" data-nama=\"". $row->name ."\"><i class=\"glyphicon glyphicon-trash\"></i> Hapus</a>
                ";
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function getAddPermission($id) {
        $module = Module::find(Hasher::decode($id));

        if (isset($module)) {
            return view('configs.modules.addPermission', compact('module'));
        }
        return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }

    public function postAddPermission(Request $request, $id) {
        $this->validate($request, [
            'nama' => 'required|unique:roles,name',
            'platform' => 'required',
        ]);

        $hashed_id = $id;

        $permission = new Permission();
        $permission->name = $request->nama;
        $permission->guard_name = $request->platform;
        $permission->module_id = Hasher::decode($id);
        if ($permission->save()) {
            return redirect('config/module/show/'.$hashed_id.'')->with('success','Berhasil menginput data Modul '.$request->name.'');
        }
        return redirect()->back()->with('error', 'Gagal menginput data Modul '. $request->name .'')->withInput();
    }

    public function postDeletePermission($id, $permissionId) {

        $permission = Permission::where('module_id', $id)->where('id', Hasher::decode($permissionId))->first();

        $hashed_id = Hasher::encode($id);
        if ((isset($permission)) && ($permission->delete())) {
            return redirect('config/module/show/'.$hashed_id.'')->with('success','Berhasil menginput data Hak Akses Modul '.$permission->name.'');
        }

        return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }


    public function getUpdate($id)
    {
        $module = Module::find(Hasher::decode($id));

        if (isset($module)) {
            return view('configs.modules.edit',compact('module'));
        }
        return redirect()->back()->with('error', 'data tidak ditemukan')->withInput();
    }

    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|unique:roles,name',
            'detail' => 'required|max:200',
        ]);

        $module = Module::find(Hasher::decode($id));
        $module->name = $request->nama;
        $module->detail = $request->detail;

        if ($module->save()) {
            return redirect('config/module')->with('success','Berhasil menginput data Modul '.$request->name.'');
        }
        return redirect()->back()->with('error', 'Gagal mengubah data Modul '. $request->name .'')->withInput();
    }

    public function postDelete($id)
    {

        $module = Module::find(Hasher::decode($id));
        $permission = Permission::where('module_id', $module->id)->delete();

        if ((isset($module)) && ($module->delete())) {
            // return response()->json([
            //     'status' => 200,
            //     'message' => 'Module berhasil dihapus',
            //     'data' => $module->name
            // ],200);
            return redirect('config/module')->with('success','Berhasil menghapus data Modul '.$module->name.'');
        }
        // return response()->json([
        //     'status' => 200,
        //     'message' => 'Module gagal dihapus',
        //     'data' => $module->name
        // ],200);
        return redirect()->back()->with('error', 'Gagal menghapus data Modul '. $module->name .'')->withInput();
    }
}
