<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarcodeFinal;
use DB, Datatables, Hasher, Validator, Auth, Storage;
use PDF;

class KayuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = 'Hapus Data Kayu!';
        $text = "Anda Yakin akan menghapus ini?";
        confirmDelete($title, $text);
        $lhp = BarcodeFinal::select('no_lhp')->groupBy('no_lhp','tgl_lhp')->orderBy('tgl_lhp','DESC')->get();
        $jenis = BarcodeFinal::with('jenis')->select('kd_jns_ky')->groupBy('kd_jns_ky')->get();
        $bkph = BarcodeFinal::with('lokasidkb')->select('kode_lokasi_dkb')->groupBy('kode_lokasi_dkb')->get();
        $tahun = BarcodeFinal::selectRaw('year(tgl_lhp) as tahun')->groupBy('tahun')->get();
        // return $tahun;
        return view('admin.barcode.index',compact('lhp','jenis','bkph','tahun'));
    }

    public function ajaxList(Request $request)
    {
        $data = BarcodeFinal::with('lokasidkb','lokasitpk','jenis','asal')
        ->select(DB::raw('no_lhp, tgl_lhp, kode_lokasi_dkb, kode_tpk, kd_jns_ky, asal_ky, sum(volume) as volume_lhp'))
        ->groupBy('no_lhp', 'tgl_lhp', 'kode_lokasi_dkb', 'kode_tpk', 'kd_jns_ky', 'asal_ky')
        ->get();
        $datatables = Datatables::of($data);

        return $datatables->addColumn('action', function ($row) {
            $hashed_id = $row->no_lhp;
                return "
                <a class=\"btn btn-sm btn-info\" href=\"". url('data/kayu/show/'.$hashed_id) ."\"><i class=\"fa fa-search\"></i></a>
                <a class=\"btn btn-sm btn-danger\" href=\"".url('data/kayu/delete/'.$hashed_id)."\" data-confirm-delete=\"true\"><i class=\"fa fa-trash\"></i></a>
                ";
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function destroy($id)
    {
        // return $id;
        BarcodeFinal::where('no_lhp', $id)->delete();
        return redirect()->back()->with('success', 'Data Kayu Berhasil dihapus');
    }

    public function show($id)
    {
        $barcode = BarcodeFinal::where('no_lhp', $id)->get();
        $title = $id;
        //return $barcode;
        return view('admin.barcode.show',compact('barcode', 'title'));
    }

    public function cetak_pdf(Request $request)
    {
        // return $request;
        $judul = BarcodeFinal::with('lokasi','lokasidkb')
        ->where('no_lhp',$request->no_lhp)->first();
    	$data = BarcodeFinal::with('lokasi','lokasidkb')
        ->where('no_lhp',$request->no_lhp)
        ->orderBy('dh_created_date', 'DESC')->get();
        if(isset($data)){
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                        ->loadview('admin.barcode.cetak_pdf',compact('data','judul'));
            $pdf->setPaper('A4','landscape');
            return $pdf->download('laporan-kayu.pdf');
        }
        return redirect()->back()->with('error','Data Tidak ditemukan');
    }

    public function bkph($id)
    {
        $html = "<option value=\"\">Pilih BKPH</option>";
        $data = BarcodeFinal::with('lokasidkb')
        ->select('kode_lokasi_dkb')->groupBy('kode_lokasi_dkb')
        ->where('kd_jns_ky',$id)
        ->get();
        foreach($data as $row) {
        $html .= "<option value=\"".$row->kode_lokasi_dkb."\">".$row->lokasidkb->nama_lokasi."</option>";
      }
      return response()->json(['html' => $html]);
    }

    public function tahun($id)
    {
        $html = "<option value=\"\">Pilih Tahun</option>";
        $data = BarcodeFinal::with('lokasidkb')
        ->selectRaw('year(tgl_lhp) as tahun')->groupBy('tahun')
        ->where('kode_lokasi_dkb',$id)
        ->get();
        foreach($data as $row) {
        $html .= "<option value=\"".$row->tahun."\">".$row->tahun."</option>";
      }
      return response()->json(['html' => $html]);
    }

    public function lhp($jenis,$bkph,$tahun)
    {
        $html = "<option value=\"\">Pilih Nomor LHP</option>";
        $data = BarcodeFinal::with('lokasidkb')
        ->select('no_lhp')->groupBy('no_lhp')
        ->where('kode_lokasi_dkb',$bkph)
        ->whereYear('tgl_lhp',$tahun)
        ->where('kd_jns_ky',$jenis)
        ->get();
        foreach($data as $row) {
        $html .= "<option value=\"".$row->no_lhp."\">".$row->no_lhp."</option>";
      }
      return response()->json(['html' => $html]);
    }

}
