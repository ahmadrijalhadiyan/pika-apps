<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Lokasi;

class LokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $title = 'Delete Data!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        $lokasi = DB::table('r_global_lokasi')
        ->select('kode_lokasi', 'nama_lokasi', 'jns_lokasi_id')
        ->where('jns_lokasi_id', '!=', '')
        ->get();
        // return $lokasi;
        return view('configs.lokasi.index', compact('lokasi'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $lokasi = DB::table('r_global_lokasi')
        ->where('kode_lokasi',$id)
        ->update([
            'kode_lokasi' => $request->kode_lokasi,
            'nama_lokasi' => $request->nama_lokasi,
            'jns_lokasi_id' => $request->jns_lokasi_id,
        ]);

        return redirect()->back()->with('success','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $lokasi = DB::table('r_global_lokasi')
        ->where('kode_lokasi',$id)
        ->delete();
        return redirect()->back()->with('success','Data berhasil dihapus');
    }
}
